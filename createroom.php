<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}



 $db = mysql_connect("localhost", "root", "");
 if(!$db) { echo mysql_error(); }
$select_db = mysql_select_db("cic_rooms"); 
if(!$select_db) { echo mysql_error(); }



$roomname = mysql_real_escape_string($_POST['roomname']);
$owner = $_SESSION['cic_studentId'];
date_default_timezone_set('Canada/Pacific');
$date = date("M-d-Y g:i:s");

$stringroom = preg_replace('/\s+/', '-', $roomname);

$query = mysql_query("SELECT `name` FROM `room_list` WHERE `name` = '$stringroom'");
if(mysql_num_rows($query) == 1) { 
  echo '1';
} else { 
  $query = mysql_query("INSERT INTO `room_list` (`owner`, `name`, `date`)
              VALUES ('$owner','$stringroom','$date')");


  mkdir("rooms/$stringroom");
  copy("rooms/misc/index.php", "rooms/$stringroom/index.php");
  copy("rooms/misc/postMessage.php", "rooms/$stringroom/postMessage.php");
  copy("rooms/misc/showMessages.php", "rooms/$stringroom/showMessages.php");
  copy("rooms/misc/upload.php", "rooms/$stringroom/upload.php");

  mkdir("rooms/$stringroom/js");
  mkdir("rooms/$stringroom/assignments");
  mkdir("rooms/$stringroom/quizes");
  mkdir("rooms/$stringroom/attendance");
  mkdir("rooms/$stringroom/members");
  copy("rooms/misc/js/postChat.js", "rooms/$stringroom/js/postChat.js");
  copy("rooms/misc/js/showChat.js", "rooms/$stringroom/js/showChat.js");
  copy("rooms/misc/js/jquery.js", "rooms/$stringroom/js/jquery.js");
  copy("rooms/misc/jquery.min.js", "rooms/$stringroom/jquery.min.js");
  copy("rooms/misc/jquery.Upload.js", "rooms/$stringroom/jquery.Upload.js");

  mysql_select_db("cic_room_chat") or die(mysql_error()); 

  mysql_query("CREATE TABLE `$stringroom` ( id INT AUTO_INCREMENT PRIMARY KEY, user VARCHAR(255), post VARCHAR(255), post_date VARCHAR(50))")or die(mysql_error());

  echo "done";
}

?>