<?php
	$room = $_GET['room'];
	$assignNum = $_GET['assignNum'];
	$file = $_GET['file'];
	$filename = "rooms/$room/assignments/assign_$assignNum";
	list($txt, $ext) = explode(".", $file);
	if($ext == 'rar'){
		header('Content-type: application/x-rar-compressed');
		header('Content-Disposition: attachment; filename="'.$file.'"');
    	readfile($filename."/".$file);
		exit();
	}elseif($ext == 'zip'){
		header('Content-type: application/zip');
		header('Content-Disposition: attachment; filename="'.$file.'"');
    	readfile($filename."/".$file);
		exit();	
	}elseif ($ext == 'txt') {
		header('Content-type: text/plain');
		header('Content-Disposition: attachment; filename="'.$file.'"');
    	readfile($filename."/".$file);
		exit();
	}elseif($ext == 'docx'){
		header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
		header('Content-Disposition: attachment; filename="'.$file.'"');
    	readfile($filename."/".$file);
		exit();
	}elseif($ext == 'doc'){
		header('Content-type: application/msword');
		header('Content-Disposition: attachment; filename="'.$file.'"');
    	readfile($filename."/".$file);
		exit();
	}elseif($ext == 'xls'){
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="'.$file.'"');
    	readfile($filename."/".$file);
		exit();
	}
?>