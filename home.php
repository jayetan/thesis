<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

//test 456

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}




	include("cic_db.php");
	$studentId = $_SESSION['cic_studentId'];
	$query = mysql_query("SELECT id, id_number, profession, firstname, lastname, profileurl, rank FROM users WHERE id_number = '$studentId'") or die(mysql_error());
	$row = mysql_fetch_row($query);
	$id = $row['0'];
	$id_number = $row['1'];
	$profession = $row['2'];
	$firstname = $row['3'];
	$lastname = $row['4'];
	$profileurl = $row['5'];
	$rank = $row['6'];
	$name = ucwords(strtolower("$firstname $lastname"));



?>
<!DOCTYPE html>
<html id='html'>
  <head>
  	<link rel='stylesheet' href='css/home.css' />
	<link rel="icon" type="image/png" href="assets/favicon.png" />
    <title>CIC Social Learning</title>
    <style>

    </style>
  </head>
<body id='body'>
 <div class="logo">
 <a href="home.php"><img src="assets/logocic.png" alt="Cic Logo" class="logocic"></a>
	<form action="home.php" method="POST" class="searchBar">
	<input type="search" id="search" class="closeDiv" placeholder='Search for Teachers, Students and Rooms...'onclick='clearFriendRequest()' autocomplete="off">
	<div id='searchResult'></div>
	</form>

	 <form action="home.php" method="POST" class="logout">

	 <?php 
	 echo "<a href='home.php'><div class='homeButton' title='Home'> </div></a>";
	 if(strlen($name) >= 15){
	 	echo "<a href='users/$profileurl/'>Hi! " . ucwords(strtolower($firstname)) ."</a>";
	 }else{
	 	echo "<a href='users/$profileurl/'>Hi! $name</a>";
	 }
	 
	 ?>

	 <input type="submit" value="Logout" name="logout" onClick='offline(); killFunction();'>
	 </form>
	 
 </div>
<div class="main_content">
	<section class="userNav">
	<?php

		if(file_exists("users/$profileurl/avatar.jpg")) {
			echo "<img src='users/$profileurl/avatar.jpg' width='100px' height='100px' alt='User Avatar' />";
		}else{
			echo "<img src='assets/avatar.png' width='100px' height='100px' alt='User Avatar' />";
		
		}

	?>
		<ul>
			<li class="liTitle">Favorites</li>
			<li class='liSub'><a href="users/<?php echo $profileurl; ?>/">Profile</a></li>
			<!--<li class='liSub'><a href="javascript:void(0)" onclick="messagesContainer()"> Messages<?php //echo "<span class='requestCount'></span></a>"; ?></li> -->
				<div id='requestContainer' style="clear:both;">
					<div>
						<strong>Friend Requests</strong>
						<button class="friendRequestButton" onclick="closeRequestContainer()" title="Close">X</button><br />
						<div id='requestWhoContainer'>
							<div id='ajaxFriend'><img src="assets/loading.gif" class='loadingImg' alt="loading.." /></div>
						</div>
					</div>
				</div>
			<li class='liSub'><a href="javascript:void(0)" onclick="requestContainer()">Friend Requests</a>
			<span class='requestHolder'></span>
			</li>


			

			<?php 

			if($profession == 'teacher' || $rank == 'admin') {echo "<li class='liTitle'>My Tools</li><li class='liSub'>" 


			?>

				<a href="#showForm">Create New Room</a></li>
			<?php 
			} 
			//if($rank == 'admin') {echo "<li class='liSub'><a href=''>New Accounts</a></li>";}
			?>
			<div id='friendList'></div>

		</ul>

		<div id="showForm">
			<div id="formBox">
				<div id="formTitle">
					Create New Room
				</div>

				<form action="" method="POST">
	        		<div id="formContent">
		            	Room Name: <input name="roomname" id='roomName' type="text" size="30" onKeyPress="return letternumber(event)">
		            	<br />
		            	<span id='createRoomCaption'>(Alpha Numeric and dash are only allowed)</span>
		            	<br />
						<span class="crateroom_val validation"></span>
						<span class="loading"></span>
		            	
		             </div>
		            <div id="formButtons">
		            	<a href="#close"><input type="button" value="Close" onclick="loadRooms()"></a>
		                <input name="createRoom" type="button" value="Create">
	               	</div>
	                
	      
	       </form>
	   		</div>
	   		
	   	</div>

	   		
	</div>
	</section>
		<div class="info_board">
			<aside class="info_holder">
				<section class="title">BULLETIN BOARD (ANNOUNCEMENTS)<?php if($rank == 'admin'){echo "<span id='editUpdate' onclick='editUpdate()' title='Edit'></span>";}  ?></section>
				<div class="scrollableUpdate">
				<?php include('updates.txt'); ?>
				</div>
			</aside>
			<br />
			<aside class="info_holder">
				<section class="title">ONLINE ROOMS / CHATROOMS</section>
				<span class="scrollableRooms">

				</span>
			</aside>
		
		</div>

		<div id='newAccount'>

			<div id='newAccountBox'>

			</div>

		</div>
	<section class="wall">
		<aside class="textarea">
		<textarea rows="4" class="closeDiv" id="message" name="wallpost" placeholder="Got something to say?..."></textarea>

		<input type="button" onClick="sendMessage()" value="Post" name="Post" id="postWallButton">
		</aside>
		<div class='modifyPostHolder'>
			<div class='modifyBox'>
				<section>EDIT / DELETE POST <button class='modifyClose' onclick='modifyPostClose()'>Close</button></section>
				<div class='msgModifyPostHolder'> </div>
				<div class='msgActionPostHolder'> </div>
			</div>
			
		</div>
		<div class="user_message">
			
		</div>

	</section>
<div id='messagesContainer'>
	<div id="messagesHolder">
		<div class='messagesTitle'>
			<span id='spanTitleMsg'>Messages</span><button onclick="closeMessagesContainer()">Close</button>
		</div>
		<div class='msgFrom'></div>
		<div class='msgContent'></div>
	</div>
	
</div>
</div>

<div id='msgHolder'>

</div>
<?php

if(isset($_POST['logout'])){
	include("cic_db.php");
	mysql_query("UPDATE `users` SET status='offline' WHERE id_number=$studentId");
	session_destroy();
	unset($_SESSION['log_studentId']);
	unset($_SESSION['cic_studentId']);
	header("Location: " . ".");
}
?>

<script type="text/javascript" src="js/jquery1.10.min.js"></script>
<script type="text/javascript" src="js/showMessages.js"></script>
<script type="text/javascript" src="js/postMessages.js"></script>
<script type="text/javascript" src="js/createroom.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/myHome.js"></script>
<script type="text/javascript">
	
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place

function letternumber(e)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// alphas and numbers
else if ((("abcdefghijklmnopqrstuvwxyz0123456789-").indexOf(keychar) > -1))
   return true;
else
   return false;
}

</script>

</body>
</html>