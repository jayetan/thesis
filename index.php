<!DOCTYPE html>
<!-- 
	######	##   ##	    ######    #####	  ######      ##     ##  #######    ########  #######    ##    ##    ######   ########
	  ##	###  ##    ##		 ##   ##  ##    ##    ##  #  ##  ##            ##     ##    ##   ##    ##   ##           ##
	  ##	## # ##    ## #####	 ##	  ##  ##    ##    ## # # ##  #####         ##     #######    ##    ##   #######      ##
	  ##	##  ###    ##    ##	 ##   ##  ##    ##    ###   ###  ##            ##     ##    ##   ##    ##        ##      ##
	######	##   ##     ######	  #####   ######      ##     ##  #######       ##     ##     ##   ######    ######       ##
-->
<html>
  <head>
  <link rel='stylesheet' href='css/index.css' />
  <link rel="icon" type="image/png" href="assets/favicon.png" />
    <title>CIC Social Learning</title>
  </head>
  <body>
<div class="loginFormDiv">
		<span class="caption">CIC Social Learning</span>
		<form action="." method="POST" class="loginForm">
			ID Number: <input type="text" name="logStudentId"> 
			Password: <input type="password" name="loginPassword"> <input type="submit" name="login" value="Login">
		</form>
</div>
<?php
include("cic_db.php");
	if(isset($_POST['login'])){
		$studentId = $_POST['logStudentId'];
		$password = $_POST['loginPassword'];
		$encryptMd = md5($password);
		$encryptSha = sha1($encryptMd);
		$studentIdFiltered = mysql_real_escape_string($studentId);
			$query = mysql_query("SELECT * FROM users WHERE id_number = '$studentIdFiltered' AND password = '$encryptSha'") or die(mysql_error());

	while($row = mysql_fetch_array($query)){
		$activated = $row['activated'];

	}
				
			if(mysql_num_rows($query) !== 0 && $activated == 'no') {
				//session_start();

				//$_SESSION['cic_studentId']=$studentId;
				//$_SESSION['log_studentId']=$studentId;
				//mysql_query("UPDATE `users` SET status='online' WHERE id_number=$studentId"); 
				header("Location: " . "login.php?id=inactive");

			}elseif(mysql_num_rows($query) !== 0 && $activated == 'yes') {
				session_start();

				$_SESSION['cic_studentId']=$studentId;
				$_SESSION['log_studentId']=$studentId;
				mysql_query("UPDATE `users` SET status='online' WHERE id_number=$studentId"); 
				header("Location: " . "home.php");

			}elseif(mysql_num_rows($query) == 0){
				header("Location: " . "login.php?id=error");

			}
		
	}


?>

<p class="quote">"Where Studying can be INTERESTING"</p>
	<span class="registerCaption">
	<p>Join Now</p><br />
		<ul>
			<li>Connect with CIC Community</li>
			<li>Submit your Homework</li>
			<li>Chat with your Classmates</li>
			<li>And Many More!</li>
		</ul>
	</span>
	<form action="." method="POST" class="registrationForm" name="registrationForm" onsubmit="return CheckData()">
		<table class="registration">	
				<tr>
					<td>
						ID Number: 
					</td>
					<td>
						<input type="text" name="studentId" value="<?php if (isset($_POST['Register'])) {echo $_POST['studentId'];} ?>">
					</td>
				</tr>
				<tr>
					<td>
						I'm a: 
					</td>
					<td>
						<select name="profession">
							<option value="">Select</option>
							<option value="teacher">Teacher</option>
							<option value="student">Student</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						Course: 
					</td>
					<td>
						<select name="course">
							<option value="">Select</option>
							<option value="BSN">BSN</option>
							<option value="BSHRM">BSHRM</option>
							<option value="BSIT">BSIT</option>
							<option value="BSCS">BSCS</option>
							<option value="BSA">BSA</option>
							<option value="BSSW">BSSW</option>
							<option value="BAPS">BAPS</option>
							<option value="BAC">BAC</option>
							<option value="BSBA">BSBA</option>
							<option value="BSED">BSED</option>
							<option value="BEED">BEED</option>
						</select>
						
					</td>
				</tr>
				<tr>
					<td>
						First Name: 
					</td>
					<td>
						<input type="text" name="firstName" value="<?php if (isset($_POST['Register'])) {echo $_POST['firstName'];} ?>">
					</td>
				</tr>
				<tr>
					<td>
						Last Name: 
					</td>
					<td>
						<input type="text" name="lastName" value="<?php if (isset($_POST['Register'])) {echo $_POST['lastName'];} ?>">
					</td>
				</tr>
				</tr>
				<tr>
					<td>
						Gender: 
					</td>
					<td>
						<select name="gender" selected="selected">
							<option value="">Select</option>
							<option value="male">Male</option>
							<option value="female">Female</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						Email: 
					</td>
					<td>
						<input type="text" id='email' name="email">
					</td>
				</tr>
				<tr>
					<td>
						Password: 
					</td>
					<td>
						<input type="password" name="newPassword">
					</td>
				</tr>
				<tr>
					<td>
						Repeat Password: 
					</td>
					<td>
						<input type="password" name="repeatPassword">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
					<input type="submit" value="Register" name="Register">
					<input type="reset" value="Clear">
					</td>
				</tr>
				</table>
				<div id="error">
					<span class="errorMessage"></span>
				</div>
				<?php
				include("cic_db.php");
					if(isset($_POST['Register'])){
						$studentId = $_POST['studentId'];
						$profession = $_POST['profession'];
						$course = mysql_real_escape_string($_POST['course']);
						$firstName = mysql_real_escape_string($_POST['firstName']);
						$lastName = mysql_real_escape_string($_POST['lastName']);
						$gender = $_POST['gender'];
						$email = mysql_real_escape_string($_POST['email']);
						$newPassword = $_POST['newPassword'];
						$repeatPassword = $_POST['repeatPassword'];
						$encryptMd = md5($newPassword);
						$encryptSha = sha1($encryptMd);
						$profileurl = md5($studentId);

						if (filter_var($email, FILTER_VALIDATE_EMAIL)){
							if($newPassword == $repeatPassword){
									if (strlen(preg_replace('/\s+/u','',$newPassword)) !== 0) {
										$query = mysql_query("SELECT * FROM users WHERE id_number = '$studentId'");
										if(mysql_num_rows($query) == 0){

												mysql_query("INSERT INTO users (id_number, profession, course, firstname, lastname, gender, email, password, profileurl, activated) VALUES('$studentId', '$profession', '$course', '$firstName', '$lastName', '$gender', '$email', '$encryptSha', '$profileurl', 'yes')");
												mkdir("users/$profileurl");
												copy("users/misc/index.php", "users/$profileurl/index.php");
												copy("users/misc/jquery.min.js", "users/$profileurl/jquery.min.js");
												copy("users/misc/jquery.Upload.js", "users/$profileurl/jquery.Upload.js");
												mysql_select_db("cic_registered_users") or die(mysql_error()); 
												mysql_query("CREATE TABLE `$studentId` ( id INT AUTO_INCREMENT PRIMARY KEY, friends INT, status VARCHAR(50), requestby INT)")or die(mysql_error());

												mysql_query("INSERT INTO `$studentId` (friends, status) VALUES('$studentId', 'yes')");

												header("Location: " . "login.php");

										}else{
											echo "<span class='errMsgRegistered'>ID Already Registered!</span>";
										}
									}else{
										echo "Password cannot be empty!";
									}
							}else{
								echo "Password Mismatch!";
							}
						}else{
							echo 'Invalid Email!';
						}

					}
				?>
		</form>
<footer>
&copy;2014 Cic Social Learning created by: Jaye Tan, Jvianney Delos Reyes, Jedrek Angeles, Zybren Carbonilla
</footer>
<script type="text/javascript" src="js/jquery1.10.min.js"></script>
<script type="text/javascript" src="validate.js"></script>
</body>
</html>