<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
$id_number = $_SESSION['cic_studentId'];

$arrayFile = $_POST["arrFiles"];
$arrRadio = $_POST["arrRadio"];
$arrInputa = $_POST["arrInputa"];
$arrInputb = $_POST["arrInputb"];
$arrInputc = $_POST["arrInputc"];
$deadline = $_POST['deadline'];
$filterDeadline = str_replace("_","/",$deadline);
$roomName = $_POST['roomName'];

$contQuestion = array();
$ansKeyRadio = array();
$choicea = array();
$choiceb = array();
$choicec = array();

foreach($arrayFile as $key => $question) {
   $contQuestion[] = $question[0];
}

foreach($arrRadio as $key2 => $answer) {
   $ansKeyRadio[] = $answer[0];
}

foreach($arrInputa as $key3 => $choice1) {
   $choicea[] = $choice1[0];
}

foreach($arrInputb as $key4 => $choice2) {
   $choiceb[] = $choice2[0];
}
foreach($arrInputc as $key5 => $choice3) {
   $choicec[] = $choice3[0];
}

$question = serialize($contQuestion);
$answerKey = serialize($ansKeyRadio);
$inputa = serialize($choicea);
$inputb = serialize($choiceb);
$inputc = serialize($choicec);

mysql_connect('localhost', 'root', '');
mysql_select_db('cic_room_tools');

$query = mysql_query("SELECT * FROM quiz_db WHERE room_name = '$roomName' AND owner = '$id_number' ORDER BY id DESC LIMIT 1");
$row = mysql_fetch_assoc($query);
$quizNum = $row['quiz_id'] + 1;

date_default_timezone_set('Canada/Pacific');
$dateNow = date('m/d/Y');



$query = mysql_query("INSERT INTO quiz_db (quiz_id, owner, room_name, question, answer, first_choice, second_choice, third_choice, deadline) VALUES('$quizNum', '$id_number', '$roomName', '$question', '$answerKey', '$inputa', '$inputb', '$inputc', '$deadline')");





?>
