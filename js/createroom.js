jQuery(function($) {
	var val_holder;

	$("form input[name='createRoom']").click(function() {

		
		val_holder 		= 0;
		var roomname 	= jQuery.trim($("form input[name='roomname']").val());

		if(roomname == "") {
			$("span.crateroom_val").html("<img src='assets/cross.png' width='10' height='10'> Field is empty.");
		val_holder = 1;
		}

		if(val_holder == 1) {
			return false;
		}
		val_holder = 0;


		$("span.loading").html("<img src='assets/loading.gif' width='30' height='30'>");
		$("span.validation").html("");

		var datastring = 'roomname='+ roomname; 


		$.ajax({
					type: "POST", 
					url: "./createroom.php",
					data: datastring, 
					success: function(responseText) { 
						if(responseText == 1) { 
							$("span.crateroom_val").html("<img src='assets/cross.png' width='10' height='10'> Room Name Already exist.");
							$("span.loading").html("");
						} else { 
							if(responseText == "done") {
								$("span.loading").html("<img src='assets/check.png' width='10' height='10'> Room Sucessfully Created.");
								$("span.validation").html("");
								$("form input[type='text']").val(''); 
								 
							}
						}
					} 
		}); 
		
	});
});
