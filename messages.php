<style>
.msgLi{
	display: block;
	padding-bottom: 5px;
}
.msgLi img{
	float: left;
	padding-right: 5px;
}
.senderName{
	font-weight: bold;
	color: #fbac1b;
}
.msgClass{
	cursor: pointer;
}
.msgClass:hover{
	background-color: #F0F0F0;
}
</style>
<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

	$id_number = $_SESSION['cic_studentId'];

mysql_connect('localhost', 'root', '');
mysql_select_db('cic_privatemessages');

$queryMembers = mysql_query("SELECT * FROM members WHERE receiver = $id_number ORDER BY `date_posted` DESC");

	
	while($row = mysql_fetch_array($queryMembers)){
		echo "<ul>";
		$sender =$row['sender'];
		$msgId = $row['message_id'];
		include('cic_db.php');
		$queryUser = mysql_query("SELECT firstname, lastname, profileurl FROM users WHERE id_number = $sender ");
		$row = mysql_fetch_assoc($queryUser);
		$name = $row['firstname'];
		$profileurl = $row['profileurl'];
		$lastname = $row['lastname'];
		$fullname = ucwords(strtolower("$name $lastname"));

		mysql_select_db('cic_privatemessages');
		$queryMessages = mysql_query("SELECT * FROM messages WHERE message_id = '$msgId'");
		$rowMsg = mysql_fetch_assoc($queryMessages);
		$msg = $rowMsg['message'];
		$msgId = $rowMsg['message_id'];
		echo "<div id=$msgId class='msgClass' onclick='showMsgContent(this.id)'>";
		echo "<li class='msgLi'>"; 

		if(file_exists("users/$profileurl/avatar.jpg")) {
			echo "<img src='users/$profileurl/avatar.jpg' width='50px' height='50px' alt='User Avatar' />";
		}else{
			echo "<img src='assets/avatar.png' width='50px' height='50px' alt='User Avatar' />";
		}

		echo "<span class='senderName'>". $fullname. "</span><span class='senderMsg'><br />". $msg ."</span></li>";
		echo "</ul><br /><hr />";
		echo "</div>";
	}
	



?>