<style>

#preview
{
color:#cc0000;
font-size:12px;
text-align: center;
}
#imageform{
	padding-left: 5px;
}
.upload_form button{
	float: right;
	
	padding-right: 5px;
	border: solid 1px;
}
.testi{
	margin: 2px; 
	width: 336px; 
	height: 38px;
	resize: none; 
	font-size: 14px;
}
.modifyPostHolder{
	display: none;
	position: absolute;
	left: 0;
	top: 0;
	height: 100%;
	width: 100%;

}

.modifyPostHolder .modifyBox{
	margin-top: 15%;
	margin-left: 30%;
	position: fixed;
	z-index: 5;
	background-color: white;
	border-radius: 5px;
	-webkit-box-shadow: 0 0 5px #888888;
    box-shadow: 0 0 5px #888888;
    width: 350px;
    height: 300px;
}
.modifyPostHolder .modifyBox section{
	font-weight: bold;
	color: #800;
	padding-left: 5px;
	padding-top: 5px;
	height: 25px;
	border-bottom: 1px solid #888888;
}
.modifyPostHolder .modifyBox .modifyClose{
	float: right;
}
.wallEditDelete{
	width: 350px;
	height: 80px;
	resize: none;
}
.btnActions{
	text-align: center;
}
.msgActionPostHolder{
	text-align: center;
	font-style: italic;
	display: none;
	padding: 10px;
}
/*.testimonialHolder{
	height: 60%;
	width: 100%;
	border-radius: 5px;
	-webkit-box-shadow: 0 0 5px #888888;
    box-shadow: 0 0 5px #888888;
}
.testimonialHolder2{
	height: 65%;
	width: 180%;
	border-radius: 5px;
	-webkit-box-shadow: 0 0 5px #888888;
    box-shadow: 0 0 5px #888888;
}*/
.sentTesti{
	width: 336px;
	height: 38px;
	margin: 2px;
	text-align: center;
}
</style>
<?php

if(isset($_POST['logout'])){
	include("cic_db.php");
	mysql_query("UPDATE `users` SET status='offline' WHERE id_number=$studentId");
	session_destroy();
	unset($_SESSION['log_studentId']);
	unset($_SESSION['cic_studentId']);
	header("Location: " . ".");
}
?>
<!DOCTYPE html>
<html id='html'>
  <head>
  	<link rel='stylesheet' href='../../css/profile.css' />
	<link rel="icon" type="image/png" href="../../assets/favicon.png" />
    <title><?php 		
			echo ucfirst("$firstname ") . ucfirst("$lastname") . "'s"; 
			?>
			 Profile
	</title>
	<style>

	</style>
  </head>

<body>
 <div class="logo">
 <a href="../../home.php"><img src="../../assets/logocic.png" alt="Cic Logo" class="logocic"></a>
	<form action="." method="POST" class="searchBar">
	<input type="search" id="search" placeholder='Search For Teachers, Students and Rooms...'  autocomplete="off" />
	<div id='searchResult'></div>
	</form>
		
	 <form action="." method="POST" class="logout">
	 <?php
		$studentId = $_SESSION['cic_studentId'];
		$query = mysql_query("SELECT firstname, lastname, profileurl FROM users WHERE id_number = '$studentId'") or die(mysql_error());
		$row = mysql_fetch_row($query);
		$firstname = $row['0'];
		$lastname = $row['1'];
		$currentUser = $row['2'];
		$stringFirstName = strtolower($firstname);
		$stringLastName = strtolower($lastname);
		$name = ucwords(strtolower("$firstname $lastname"));
		 echo "<a href='../../home.php'><div class='homeButton' title='Home'> </div></a>";

		if(strlen($name) >= 15){
	 		echo "<a href='../../users/$currentUser'>Hi! " . ucwords(strtolower($firstname)) ."</a>";
		 }else{
	 		echo "<a href='../../users/$currentUser'>Hi! $name</a>";
		 }
	 ?>
	 <input type="submit" value="Logout" name="Logout">
	 </form>
	 
</div>
<div class="main_content">
<section class="userNav">
<?php
			$studentId = $_SESSION['cic_studentId'];
			$query = mysql_query("SELECT id, id_number, profession, course, firstname, lastname, gender, profileurl FROM users WHERE profileurl = '$profileurl'") or die(mysql_error());
			$row = mysql_fetch_row($query);
			$id = $row['0'];
			$id_number = $row['1'];
			$profession = $row['2'];
			$course = $row['3'];
			$profilefirstname = $row['4'];
			$profilelastname = $row['5'];
			$gender = $row['6'];
			$profileurl = $row['7'];
			$stringFirstName = strtolower($profilefirstname);
			$stringLastName = strtolower($profilelastname);
			

?>
<span class='imageHolder'><?php	if(file_exists("../../users/$profileurl/avatar.jpg")) {
		echo "<img src='../../users/$profileurl/avatar.jpg' width='150' height='150' alt='User Avatar' />";
	}else{
		echo "<img src='../../assets/avatar.png' width='150' height='150px' alt='User Avatar' />";
	}
 ?></span>
<?php
	$currentPage = basename($_SERVER['REQUEST_URI']);

	if (preg_match('/\?/', $currentPage)){
	$path = dirname($_SERVER['REQUEST_URI']);
	$patharr = explode('/', $path);
	$count = count($patharr);
	$profilePage = $patharr[4-1];
	}else{
		$profilePage = basename($_SERVER['REQUEST_URI']);
	}

	
	if(md5($studentId) == $profilePage){
?>
	<div id="prof_btn">
	<button class="profile_button">Update Profile Photo</button>
	</div>

<?php
}
?>


</section>
<div class="middle_info">
	<section class="about">
		ABOUT
	</section>
		<ul>
			<li>Name: <?php echo ucwords("$stringFirstName ") . ucfirst("$stringLastName"); ?></li>
			<li>Course: <?php echo strtoupper($course); ?><li>
			<li>Gender: <?php echo ucwords($gender); ?></li>
		</ul>
	<section class="testimonials">
	<!--	TESTIMONIALS<span class='counterTesti' style='font-size:12;color:white;background-color:#800;'></span> -->
	</section>
	<?php
		mysql_connect('localhost', 'root','');
		mysql_select_db("cic_testimonials");
		$queryTesti = mysql_query("SELECT * FROM testimonials WHERE sender = $studentId AND id_number = '$profilePage'") OR die(mysql_error());
		$queryFetch = mysql_fetch_row($queryTesti);
		
	if(md5($studentId) != $profilePage && $queryFetch == 0){
			mysql_connect('localhost', 'root', '');
			mysql_select_db('cic_registered_users');
			$friendQuery = mysql_query("SELECT id, friends, status, requestby FROM `$studentId` WHERE `friends` = '$id_number'");
			$rowFriend = mysql_fetch_row($friendQuery);
			$friendStatus = $rowFriend['2'];
			$requestby = $rowFriend['3'];
			if(mysql_num_rows($friendQuery) == 1 && $friendStatus == 'yes'){
				echo "<div class='testiTextBoxHolder'>";

		//		echo "<input class='testi' id='testiTextbox' placeholder='Type your testimonial and press enter..' />";
				echo"</div>";
				echo "<div class='testimonialHolder'>";
				mysql_select_db("cic_testimonials");
				$queryTesti = mysql_query("SELECT * FROM testimonials WHERE profileurl = '$profilePage'") OR die(mysql_error());
				while($fetchTesti = mysql_fetch_array($queryTesti)){
					echo $message = $fetchTesti['message']."<br />";

				}
				echo "</div>";

			}else{


				}

			}elseif(md5($studentId) == $profilePage){
				echo "<div class='testimonialHolder2'>";
				$queryTesti = mysql_query("SELECT * FROM testimonials WHERE id_number = '$studentId'") OR die(mysql_error());
				while($fetchTesti = mysql_fetch_array($queryTesti)){
					echo $message = $fetchTesti['message']."<br />";
				}
				echo "</div>";
			}

	?>
</div>
<div id="activity">

<?php



?>
	<aside>
	
</div>
</div>
<div class="profile_form">
	<div class="form_box">
		<div class="upload_form">
				<div class="uploadTitle">Upload Profile Photo<button class="profile_button" onclick="reloadPage()">Close</button></div>
				<div class="uploadContent">
					<div>

						<form id="imageform" method="post" enctype="multipart/form-data" action='../../uploadProfile.php'>
						
						Upload your image <input type="file" name="photoimg" id="photoimg" />
						</form>
						<div id='preview'>
						</div>
					</div>
		
		</div>
	</div>
</div>
<div class='modifyPostHolder'>
			<div class='modifyBox'>
				<section>EDIT / DELETE POST <button class='modifyClose' onclick='modifyPostClose()'>Close</button></section>
				<div class='msgModifyPostHolder'> </div>
				<div class='msgActionPostHolder'> </div>
			</div>
			
		</div>
<script type="text/javascript" src="../../js/jquery1.10.min.js"></script>
<script type="text/javascript" src="../../js/searchProfile.js"></script>
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="jquery.Upload.js"></script>
<script>
 $(document).ready(function() { 
		
            $('#photoimg').live('change', function(){ 
			    $("#preview").html('');
			    $("#preview").html('<img src="../../assets/ajax-loader.gif" alt="Uploading...."/>');
				$("#imageform").ajaxForm({
						target: '#preview'
				}).submit();
		
			});
        }); 
$(function () {
    $('.profile_button').click(function () {
        $('.form_box').toggle();
    })
})




function add(){
	var id_numAdd = $("#id_numAdd").val();
	$.post("../../addFriend.php", {id_numAdd: id_numAdd });
	$("#addMessage").hide().html('Friend Request Sent').fadeIn('slow');
	
}

function profile_add() {
	$("#scroll").load("showMessages.php");
}
	
function confirm(){
	$("#confirmSpan").fadeOut('slow');
	var id_numConfirm = $("#id_numConfirm").val();
	$.post("../../addFriend.php", {id_numConfirm: id_numConfirm });
	activity().fadeIn('slow');

}
function reloadPage(){
	location.reload();
}
(function userActivity(){

	$("#activity").load("../../activity.php?profile=<?php echo $profilePage; ?>");

 setTimeout(userActivity, 5000);

})();

function modifyPost(getPost_id){
	var postIdNum = getPost_id;
	var postNum = postIdNum.split(":");
	$('.modifyPostHolder').toggle();
	$(".msgModifyPostHolder").load("../../wallPostEdit.php?postId="+postNum[1]);
}
function modifyPostClose(){
		$('.modifyPostHolder').toggle();
		$(".wallEditDelete").html("");
		$(".msgActionPostHolder").html("");
		$(".msgActionPostHolder").fadeOut();
	}
function wallPostUpdate(update_id){
		var updatedPost = $(".wallEditDelete").val();
		var updateId = update_id;
		$.post("../../wallPostAction.php?action=update", {updateId: updateId, updatedPost: updatedPost });
		$(".msgActionPostHolder").html("Post Updated").fadeIn();
}
function wallPostDelete(delete_id){
		var delete_id = delete_id;
		var idArr = delete_id.split(":");
		$.post("../../wallPostAction.php?action=delete", {idDelete: idArr[1] });
		$(".msgActionPostHolder").html("Post Deleted").fadeIn();
		$(".wallEditDelete").html("");
}
$("#testiTextbox").keypress(function(e) {
  if(e.keyCode == 13){
  	var message = $( "#testiTextbox").val();
  	var receiver = "<?php echo $profilePage; ?>" ;
  	$.post("../../testimonials.php", {message: message, receiver: receiver } , function() { 
		$("#testiTextbox").val('');
		$(".testiTextBoxHolder").html("<div class='sentTesti'>Testimonial Sent, Waiting for Approval..</div>");

  	});
  }
});
</script>
</body>
</html>