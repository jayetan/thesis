<div class="main_content">
<section class="userNav">
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

	include("cic_db.php");
			$studentId = $_SESSION['cic_studentId'];
			$query = mysql_query("SELECT id, id_number, profession, course, firstname, lastname, gender, profileurl FROM users WHERE profileurl = '$profileurl'") or die(mysql_error());
			$row = mysql_fetch_row($query);
			$id = $row['0'];
			$id_number = $row['1'];
			$profession = $row['2'];
			$course = $row['3'];
			$profilefirstname = $row['4'];
			$profilelastname = $row['5'];
			$gender = $row['6'];
			$profileurl = $row['7'];
			$stringFirstName = strtolower($profilefirstname);
			$stringLastName = strtolower($profilelastname);
			var_dump($id);
			
	if(file_exists("../../users/$profileurl/avatar.jpg")) {
		echo "<img src='../../users/$profileurl/avatar.jpg' width='150' height='150' alt='User Avatar' />";
	}else{
		echo "<img src='../../assets/avatar.png' width='150' height='150px' alt='User Avatar' />";
	}

?>
<?php
	$profilePage = basename($_SERVER['REQUEST_URI']);

	if(md5($studentId) == $profilePage){
?>
	<div id="prof_btn">
	<button class="profile_button">Update Profile Photo</button>
	</div>

<?php
}
?>

<ul>
	<li>BIO</li>
	<li>Name: <?php echo ucfirst("$stringFirstName ") . ucfirst("$stringLastName"); ?></li>
	<li>Course: <?php echo strtoupper($course); ?><li>
</ul>
</section>
<div class="middle_info">
	<section class="about">
		ABOUT
	</section>
	<section class="testimonials">
		TESTIMONIALS
	</section>
</div>
<div id="activity">

<?php



?>
	<aside>
	<?php 
			mysql_connect('localhost', 'root', '');
			mysql_select_db('cic_registered_users');

			$friendQuery = mysql_query("SELECT id, friends, status FROM `$studentId` WHERE `friends` = '$id'");
			$rowFriend = mysql_fetch_row($friendQuery);
			
			$friendStatus = $rowFriend['2'];
	
			if(mysql_num_rows($friendQuery) == 1 && $friendStatus == 'yes'){

				if(md5($studentId) == $profilePage){

					echo "MY ACTIVITY";

				}else{
					echo strtoupper("$stringFirstName ") . strtoupper("$stringLastName") . "'S' ACTIVITY"; 
				}



			
		
	echo "</aside>";

include("cic_db.php");

$id_number = $_SESSION['cic_studentId'];

	$query = mysql_query("SELECT * FROM `wall` WHERE `id_number` = '$id' ORDER BY id DESC");

	echo "<div id='scrollable'>";


	while($row = mysql_fetch_assoc($query)){
		$message = $row['post'];
	echo "<div class='user_box'><span class='image'>";
		if(file_exists("../../users/md5($studentId)/avatar.jpg")) {
				echo "<img src='../../users/md5($studentId)/avatar.jpg' width='50px' height='50px' alt='User Avatar' />";
			}else{
				echo "<img src='../../assets/avatar.png' width='50px' height='50px' alt='User Avatar' />";
			}

			
			
			
			echo "</span><strong>" . ucfirst("$stringFirstName ") . " " . ucfirst("$stringLastName") . "</strong><br /><br /><span class='user_post'>" . $row['post']. "<br /><br /><span class='time'>". $row['time'];
			echo "</span></span></div><br /><hr />";
		}

		}elseif(mysql_num_rows($friendQuery) == 1 && $friendStatus == 'pending'){

			echo "Pending Friend Request <button type='button' disabled>Confirm</button>";

		}else{
				echo "Only Friends of ".  strtoupper("$stringFirstName ") . strtoupper("$stringLastName") . " can view "; 
					if($gender == 'male'){
						echo "his Activity";
					}else{
						echo "her Activity";
					}
				echo "<br /><input type='hidden' id='id_numAdd' value='$id' />
					  <button onClick='add()'>Add as a Friend</button>";
			}
	echo "</div>";
	?>
</div>
</div>
<div class="profile_form">
	<div class="form_box">
		<div class="upload_form">
			<form action="" method="POST" enctype="multipart/form-data">
				<div class="uploadTitle">Upload Profile Photo</div>
				<div class="uploadContent">
					<label for="file">Profile Image: </label>
					<input type="file" name="imageFile" id="file"><br>
					<span class="uploadImage validation"></span>
					<span class="loading"></span>
					<br />
				</div>
				<div class="uploadBottons">
					<button class="profile_button">Cancel</button>
					<input type="submit" type="button" name="uploadProfilePhoto" value="Upload"></input>
				</div>
			</form>
		
		</div>
	</div>
</div>