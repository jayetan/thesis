<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

?>
<?php 
include("cic_db.php");
$splitId = $_POST['splitId'];
$id_number = $_SESSION['cic_studentId'];

	$query = "SELECT cic_registered_users.$id_number.friends, cic_registered_users.$id_number.status, cic_db.wall.id, cic_db.wall.id_number, cic_db.wall.post, cic_db.wall.time ".
	 	"FROM cic_registered_users.$id_number, cic_db.wall ".
		"WHERE cic_registered_users.$id_number.friends = cic_db.wall.id_number AND status = 'yes'AND cic_db.wall.id > '$splitId' ORDER BY id DESC";
		 
	$result = mysql_query($query) or die(mysql_error());


	while($row = mysql_fetch_array($result)){

		$queryName = mysql_query("SELECT firstname, lastname, profileurl, rank FROM users WHERE id_number = '$row[id_number]'") or die(mysql_error());

		$rowName = mysql_fetch_row($queryName);
    

    echo "<div class='user_info $row[id]'>";
     if($row['status'] == "yes"){
			echo "<a href = 'users/$rowName[2]'>";
			if(file_exists("users/$rowName[2]/avatar.jpg")) {
				echo "<img src='users/$rowName[2]/avatar.jpg' width='50px' height='50px' alt='User Avatar' />";
			}else{
				echo "<img src='assets/avatar.png' width='50px' height='50px' alt='User Avatar' />";
			}
			echo "<strong>" . ucwords(strtolower($rowName['0'])) . " " . ucwords(strtolower($rowName['1'])) . "</strong></a>";
      if($id_number == $row['id_number']){

     echo "<span title='Edit/Delete' id=post:$row[id] onclick=modifyPost(this.id) style='float:right;width: 10px;width: 0;height: 0; border-left: 7px solid transparent; border-right: 7px solid transparent; border-top: 7px solid gray;border-bottom: 0;cursor:pointer;'></span>";
     }
      echo"<br /><br /><span class='user_post'>" . $row['post']. "<br /><br /><span class='time'>". $row['time'];
			echo "</span></span></div>";
		}
	}
?>
