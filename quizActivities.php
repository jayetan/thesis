<style type="text/css">
	.quizDeadline{
		font-size: 10px;
		font-style: italic;
	}
	.quizDeadline:hover{
		color: #FBAC1B;
		cursor: pointer;
	}

</style>

<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

$id_number = $_SESSION['cic_studentId'];
$roomName = $_GET['room'];
$action = $_GET['action'];

mysql_connect('localhost', 'root', '');
mysql_select_db('cic_room_tools');
date_default_timezone_set('Canada/Pacific');
$date = date('m/d/Y');
$dateToday = strtotime('$date');
$queryRoom = mysql_query("SELECT * FROM quiz_db WHERE room_name = '$roomName'")or die(mysql_error());

if($action == 'viewQuiz'){
	while ($row = mysql_fetch_array($queryRoom)) {
	$deadline = $row['deadline'];
	$filterDeadline = str_replace('_', '/', $deadline);

	echo "Quiz ".$quizNum = $row['quiz_id'];
	echo  "<span class='quizDeadline' id='$quizNum' onclick='launchQuiz(this.id)'> Deadline: ". $filterDeadline."view</span><br />";
	}

}elseif($action == 'takeQuiz') {
$roomId = $_GET['quizId'];
$queryQuiz = mysql_query("SELECT * FROM quiz_scores WHERE room = '$roomName' AND quiz_id = '$roomId' AND id_number = '$id_number'") or die(mysql_error());
$queryRoom = mysql_query("SELECT * FROM quiz_db WHERE room_name = '$roomName'")or die(mysql_error());
	while ($row = mysql_fetch_array($queryRoom)) {
	$deadline = $row['deadline'];
	$filterDeadline = str_replace('_', '/', $deadline);
}
if(mysql_num_rows($queryQuiz) == 1){
	echo "You Have already Taken This Quiz";
}elseif($date >= $filterDeadline){
		echo "Quiz already Expired!";
		var_dump(($date <= $filterDeadline));

	}else{
	echo "<input type=hidden id=roomIdNum value=$roomId>";
	$x = 0;

	$query = mysql_query("SELECT * FROM quiz_db WHERE room_name ='$roomName' AND quiz_id = $roomId") or die(mysql_error());
	while($fetch = mysql_fetch_assoc($query)){
		$array = unserialize($fetch['question']);
		$answer = unserialize($fetch['answer']);
		$firstchoice = unserialize($fetch['first_choice']);
		$secondchoice = unserialize($fetch['second_choice']);
		$thirdchoice = unserialize($fetch['third_choice']);
	}
	foreach($array as $question){
		echo "Question ".($x+1).".<br />".$array[$x];
		echo "<br />A. <input type='radio' name='answer$x' id='radio$x' value='a'>".$firstchoice[$x];
		echo "<br />B. <input type='radio' name='answer$x' id='radio$x' value='b'>".$secondchoice[$x];
		echo "<br />C. <input type='radio' name='answer$x' id='radio$x' value='c'>".$thirdchoice[$x];
		echo "<br /><br />";
		$x++;
	}
	echo "<input type='submit' value='submit' onclick='sendValue()' /></form>";
	$count = count($array);
	}
}

?>
<script>

var  i = 0;
var files = "<?php echo $count; ?>";
 
function sendValue(){

var arrRadio = jQuery.makeArray();


while (i <= files) {

 var radiovalue = [$("input[name=answer"+i+"]:checked").val()];


 arrRadio.push(radiovalue);

  i++;
  }
  alert(arrRadio);
  $.post("../../answer.php",{arrRadio:arrRadio});
  $(".quizFormHolderContent").hide();
  showScore();

 }
 function showScore(){
 	var roomName = "<?php echo $roomName;?>";
 	var quizId = $('#roomIdNum').val();
 	var fetchScore = "../../quizScore.php?room="+roomName+"&quizId="+quizId;
	$('.ScoreHolder').fadeIn();
	$('.ScoreHolder').load(fetchScore);
 }
</script>