<style>
#quizForm{
	padding-left: 10px;
	padding-bottom: 10px;
	clear: both;
}
#quizGenHolder{
	padding-left: 40px;
	border: solid 1px;
}
#quizForm textarea{
    border: 1px solid #800; 
    -webkit-box-shadow: 
      inset 0 0 3px  rgba(0,0,0,0.1),
            0 0 5px rgba(0,0,0,0.1); 
    -moz-box-shadow: 
      inset 0 0 3px  rgba(0,0,0,0.1),
            0 0 5px rgba(0,0,0,0.1); 
    box-shadow: 
      inset 0 0 3px  rgba(0,0,0,0.1),
            0 0 5px rgba(0,0,0,0.1); 
}
#quizForm input {
    border: 1px solid #fbac1b; 
    -webkit-box-shadow: 
      inset 0 0 3px  rgba(0,0,0,0.1),
            0 0 5px rgba(0,0,0,0.1); 
    -moz-box-shadow: 
      inset 0 0 3px  rgba(0,0,0,0.1),
            0 0 5px rgba(0,0,0,0.1); 
    box-shadow: 
      inset 0 0 3px  rgba(0,0,0,0.1),
            0 0 5px rgba(0,0,0,0.1); 
    padding: 2px;
    background: rgba(255,255,255,0.5);
    margin: 0 0 10px 0;
}
.errorDiv{
	display: none;
	float: right;
	position: absolute;
	margin-left: 40%;
	top: 25%;
	border: 1px solid red;
	padding: 5px 5px 5px 5px;
	overflow: auto;
	overflow-x: hidden;
	height: 300px;
	width: 250px;
	}
.msgDiv{
	display: none;
	float: right;
	margin-left: 400px;
	top: 150px;
	overflow: auto;
	overflow-x: hidden;
	height: 300px;
	width: 250px;
	position: absolute;
}
.spanError{
	font-style: italic;
	color: red;
}
.spanMsg{
	font-style: italic;
	color: green;
	margin-left: 30%;
	top: 300px;
	border-radius: 5px;
	padding: 5px 5px;
	-webkit-box-shadow: 0 0 5px #888888;
    box-shadow: 0 0 5px #888888;
    position: absolute;
    font-size: 24px;
}
.successMsg{
	display: none;
}
</style>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

$id_number = $_SESSION['cic_studentId'];
$questionCount = $_GET['questions'];
$deadline = $_GET['deadline'];
$roomName = $_GET['roomName'];
include("cic_db.php");
$query = mysql_query("SELECT profession FROM users WHERE id_number = $id_number")or die(mysql_error());
$fetchProfession = mysql_fetch_assoc($query);

if($fetchProfession['profession'] == 'teacher'){
$x=1; 


echo "<div id='quizForm'>";
echo "<p>Use the text field to type your question, input fields for choices, and radio button for the right answer</p>";
echo "<div id='quizGenHolder'>";
while($x <= $questionCount)
  {
  echo "Question ".$x."<br /><textarea name='question$x' class='quesClass$x' placeholder='Type your Question here..' style='resize:none;height:50px;width:300px;'></textarea><br /><br />";
  echo "<input type='radio' name='answer$x' value='a' class='radio$x'> <input type='text' class='choiceClassa$x' name='choicea$x' placeholder='Choice A'><br />";
  echo "<input type='radio' name='answer$x' value='b' class='radio$x'> <input type='text' class='choiceClassb$x' name='choiceb$x' placeholder='Choice B'><br />";
  echo "<input type='radio' name='answer$x' value='c' class='radio$x'> <input type='text' class='choiceClassc$x' name='choicec$x' placeholder='Choice C'><br /><br />";
  $x++;
  } 

	echo "<button onclick='validate()'>Create</button>";
	echo "<div class='msgDiv'></div>";
}
echo "</div>";
echo "</div>";
echo "<div class='successMsg'></div>"



?>
<script>
var files = "<?php echo ($x - 1); ?>";
var  i = 1;

var inputs, index;

function validate(){
var questionVal = jQuery.makeArray();
var radio = jQuery.makeArray();
var choiceA = jQuery.makeArray();
var choiceB = jQuery.makeArray();
var choiceC = jQuery.makeArray();
var error = jQuery.makeArray();
var y = 1;

while (y <= files) {

	questionVal[y] = $(".quesClass"+y).val();
	radio[y] = $("input[name=answer"+y+"]:checked").val();
	choiceA[y] = $("input[name=choicea"+y+"]").val();
	choiceB[y] = $("input[name=choiceb"+y+"]").val();
	choiceC[y] = $("input[name=choicec"+y+"]").val();
	
	if(!questionVal[y]){
	
		error.push("Question "+[y]+" is Empty.<br />");
		}
		
	if(!radio[y]){
		error.push("No Answer provided in Question "+[y]+"<br />");
		}
		
	if(!choiceA[y]){
		error.push("Choice A is Empty in Question "+[y]+"<br />");
		}
	if(!choiceB[y]){
		error.push("Choice B is Empty in Question "+[y]+"<br />");
		}
	if(!choiceC[y]){
		error.push("Choice C is Empty in Question "+[y]+"<br />");
		}
	y++;
	
	}
	var filtererror = error.toString();
	var filteredString = filtererror.replace(/\,/g,'');
	
	if(error){
		$(".msgDiv").fadeIn(function (){ $(".msgDiv").html("<span class='spanError'>"+filteredString+"</span>"); });
	}
	
	
	
	if(error == ""){
		getValue();
	}


}

function getValue(){

	var arrFiles = jQuery.makeArray();
	var arrRadio = jQuery.makeArray();
	var arrInputa = jQuery.makeArray();
	var arrInputb = jQuery.makeArray();
	var arrInputc = jQuery.makeArray();
	var roomName = "<?php echo $roomName; ?>";
	var deadline = "<?php echo $deadline; ?>";
	$("#quizForm").hide();
	while (i <= files) {
		var filevalue = [$(".quesClass"+i).val()];
		var radiovalue = [$("input[name=answer"+i+"]:checked").val()];
		var inputvaluea = [$("input[name=choicea"+i+"]").val()];
		var inputvalueb = [$("input[name=choiceb"+i+"]").val()];
		var inputvaluec = [$("input[name=choicec"+i+"]").val()];
		arrFiles.push(filevalue);
		arrRadio.push(radiovalue);
		arrInputa.push(inputvaluea);
		arrInputb.push(inputvalueb);
		arrInputc.push(inputvaluec);
		i++;
	}

	$.post("../../insertQuiz.php",{arrFiles:arrFiles, arrRadio:arrRadio, arrInputa:arrInputa, arrInputb:arrInputb, arrInputc:arrInputc, deadline:deadline, roomName:roomName});

	$(".successMsg").fadeIn(function (){ $(".successMsg").html("<span class='spanMsg'>Quiz Created</span>"); });

	var viewQuiz = "../../quizActivities.php?action=viewQuiz&room="+roomName;
	
	$('#quizHolder').load(viewQuiz);
 }
 
</script>