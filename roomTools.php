<style>

.ui-datepicker {  
    width: 216px;  
    height: auto;  
    font: 9pt Arial, sans-serif;  
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);  
    -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);  
    box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);  
    background-color: white;
}
.ui-datepicker table {  
    width: 100%;  
    padding-left: 3px;
} 
.ui-datepicker-header {  
    color: #800;  
    font-weight: bold;  
    line-height: 30px;  
    border-width: 1px 0 0 0;  
    border-style: solid;  
    border-color: #111;  
} 
.ui-datepicker-title {  
    text-align: center;  
} 
.ui-datepicker-prev, .ui-datepicker-next {  
    display: inline-block; 
}
.ui-datepicker-prev {  
    float: left; 
    padding-left: 3px;
}  
.ui-datepicker-next {  
    float: right;
    padding-right: 3px;
} 
.ui-datepicker thead {  
    background-color: #f7f7f7;  
    background-image: -moz-linear-gradient(top,  #f7f7f7 0%, #f1f1f1 100%);  
    background-image: -webkit-gradient(linear, left top, left bottombottom, color-stop(0%,#f7f7f7), color-stop(100%,#f1f1f1));  
    background-image: -webkit-linear-gradient(top,  #f7f7f7 0%,#f1f1f1 100%);  
    background-image: -o-linear-gradient(top,  #f7f7f7 0%,#f1f1f1 100%);  
    background-image: -ms-linear-gradient(top,  #f7f7f7 0%,#f1f1f1 100%);  
    background-image: linear-gradient(top,  #f7f7f7 0%,#f1f1f1 100%);  
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f7f7f7', endColorstr='#f1f1f1',GradientType=0 );  
    border-bottom: 1px solid #bbb;  
}
.ui-datepicker tbody td {  
    padding: 0;  
    text-align: center;
    border-right: 1px solid #bbb;  

} 
.ui-datepicker tbody td:last-child {  
    border-right: 0px;  
} 
.ui-datepicker tbody tr {  
    border-bottom: 1px solid #bbb;  
}  
.ui-datepicker tbody tr:last-child {  
    border-bottom: 0px;  
}  
.ui-datepicker-calendar .ui-state-hover {  
    background: #f7f7f7;  
}
#assignNotes{
	width: 90%;
}
.assignDiv{
	clear: both;
}
.assignDiv .assignHeader{
	font-size: 14px;
	color: #800;
	font-weight: bold;
	text-align: center;
	clear: both;
	position: relative;
	background-color: #fbac1b;
	padding: 5px 0 5px 0;
	border-bottom: solid 1px #800;

}
.assignSubDiv{
	float: left;
	margin-top: 5%;
	position: relative;
	width: 50%;
	margin-left: 5px;
	text-align: center;
	padding-bottom: 5px;
	border-radius: 5px;
	-webkit-box-shadow: 0 0 5px #888888;
    box-shadow: 0 0 5px #888888;
}
.assignSubDiv p{
	border-bottom: solid 1px #888888;
}
.postedAssign{
	float: right;
	margin-top: 5%;
	position: relative;
	width: 45%;
	margin-right: 5px;
	padding-bottom: 5px;
	border-radius: 5px;
	-webkit-box-shadow: 0 0 5px #888888;
    box-shadow: 0 0 5px #888888;
}
.postedAssign p{
	text-align: center;
	border-bottom: solid 1px #888888;
}
.currentAssign{
	float: left;
	padding-left: 5px;
	height: 144px;
	overflow: auto;
	overflow-x: hidden;
	width: 98%;
	margin-top: -10px;
	padding-bottom: 8px;
}
.assignButton{
	text-align: center;
	position: relative;
	padding-top: 10%;
	clear: both;
}
#closeAssignButton{
	margin-top: 80%;
	position: relative;
}
#assignMsg{
	font-style: italic;
}
.editAssign{
	font-size: 10px;
	font-style: italic;
	cursor: pointer;
}
.editAssign:hover{
	color: #fbac1b;
}
.editActivity{
	display: none;
	position: absolute;
	margin-left: 25%;
	background-color: white;
	width: 300px;
	height: 300px;
	border: solid 1px;
	-webkit-box-shadow: 0 8px 6px -6px black;
    -moz-box-shadow: 0 8px 6px -6px black;
        box-shadow: 0 8px 6px -6px black;

}
#activityId{
	position: relative;
	text-align: center;
	border-bottom: 1px solid;
	padding-bottom: 10px;

}
#activityId span{
margin-left: 40px;
}
.assignContentEdit{
	text-align: center;
}
.quizTitle{
	border-bottom: 1px solid;
	background-color: #fbac1b;
	font-weight: bold;
	font-size: 14px;
	color: #800;
	height: 25px;
	clear: both;
}
.quizTitle p{
padding: 5px 0 0 5px;
}
.quizCloseButton{
	margin-top: -33px;
	float: right;
	position: relative;
}
.quizHolder{
	height: 95%;
	border: 1px solid;
	width: 65%;
	overflow: auto;
	overflow-x: hidden;
}
.datePickerHolder{
	float: right;
	border: solid 1px;
}
</style>
<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}


$studentId = $_SESSION['cic_studentId'];
$roomName = $_GET['roomName'];
$create = $_GET['create'];

mysql_connect('localhost', 'root', '');
mysql_select_db('cic_rooms');

$query = mysql_query("SELECT * FROM room_list WHERE owner = '$studentId' AND name = '$roomName'");
	if(mysql_num_rows($query) != 0){
		if($create == 'assign'){
			?>
			<div class='assignDiv'>
			<p class='assignHeader'>CREATE / EDIT ASSIGNMENTS FOR YOUR STUDENTS</p>
			<section class='assignSubDiv'>
			
			<p>Assignment Form: 

			<?php 
				mysql_connect('localhost', 'root', '');
				mysql_select_db('cic_room_tools');

				$query = mysql_query("SELECT * FROM assignments WHERE room_name = '$roomName' AND owner = '$studentId' ORDER BY id DESC LIMIT 1");
				$row = mysql_fetch_assoc($query);
				$assignNum = $row['assign_number'] + 1;
				if(mysql_num_rows($query) != 0){
					echo "Assignment #" . $assignNum;

				}

			?>

			</p>

			<textarea id='assignNotes' placeholder='Type Your Notes Here..'></textarea>
			<br />
			<br />
			End of Submission: <input type='text' class='datepicker' placeholder='MM/DD/YYYY'>
			<br />
			<br />
			<br />
			<span id='assignMsg'></span>
			<br />
			<button onclick='postAssign()'>Process</button> <button onclick='showAssign(), reloadAssign()'>Done</button>
			
			</section>
			<section class='postedAssign'>
					  <p>Currently Created Assignments:</p>
					  <div class='currentAssign'>


					  </div>

			<div class='editActivity'>
				<div id='activityId'></div>
				<div class='assignContentEdit'></div>
			</div>

			</section>
			<div class="assignButton"><button onclick='showAssign(), reloadAssign()'>Close</button> </div>
			</div>
			<?php
		}elseif($create == 'quiz'){
			echo "<div class='quizTitle'><p>QUIZ MAKER</p>";
			echo "<button class='quizCloseButton' onclick='createQuiz()'>Cancel</button>";
			echo "</div>";
			echo "<div class='quizMakerHolder'>";
			echo "<div class='quizHolder'>";
			echo $assignNum;
			echo "</div>";

			echo "<div class='datePickerHolder'>";
			echo "End of Submission: <input type='text' class='datepicker' placeholder='Select Date'>";
			echo "</div>";
			echo "</div>";

		}elseif($create == 'records'){
			echo "RECORDS";
			echo "<button onclick='viewRecord()'>Cancel</button>";
		}

	}

?>

<script>

var roomName = "<?php echo $roomName; ?>";
var pathActivity = '../../roomActivities.php?action=assign&room='+roomName;

$(document).ready(function() {
	$('.currentAssign').load(pathActivity);
});


function postAssign(){
	var note = $("#assignNotes").val();
	var folderNumber = <?php echo $assignNum; ?>;
	var deadline = $(".datepicker").val();
	if(note && deadline){

var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;

		if(pattern.test(deadline)){
				var splitDate = deadline.split("/");
				if(splitDate[0] > 12){
					$("#assignMsg").html('<h4 style="color: red;">Invalid Date Format</h4>');
				}else if(splitDate[1] > 31){
					$("#assignMsg").html('<h4 style="color: red;">Invalid Date Format</h4>');
				}else{
					var path = '../../roomToolsProcess.php?action=insert&room=<?php echo $roomName; ?>';
					$.post(path, {note: note, deadline: deadline, assign: folderNumber } , function() { clearBox(); });
				}
			}else{
				$("#assignMsg").html('<h4 style="color: red;">Invalid Date Format</h4>');
			}

	}else if(note.length == 0 || deadline.length == 0){
	var assignMsg = $("#assignMsg").html('<h4 style="color: red;">Input Field Must Not Be Empty</h4>');
	}


}


function clearBox() {
	var number = <?php echo $assignNum; ?>;
	var note = $("#assignNotes").val('');
	var deadline = $("#datepicker").val('');
	var assignMsg = $("#assignMsg").html('<h4 style="color: green;">Assignment #: ' + number+ ' Posted</h4>');
	$('.currentAssign').load(pathActivity);
 
 }
 function assignEdit(assign_id){
		var id = assign_id;
		var editPath = '../../editRoomTools.php?edit=quiz&room='+roomName+'&assign_num='+id;
        $('.editActivity').toggle();
        document.getElementById('activityId').innerHTML = "<span>Edit Assignment #: "+id+"</span><button onclick='closeEditAssign()' style='float:right;padding-top:-10px'>Close</button>";

       $('.assignContentEdit').load(editPath);
}
function closeEditAssign(){
	   $('.editActivity').toggle();
}
function reloadAssign(){
	var viewAssign = "../../roomActivities.php?action=viewAssign&room="+roomName;
	$('#assignHolder').load(viewAssign);
}
</script>