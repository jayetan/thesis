<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

$owner = $_SESSION['cic_studentId'];

$note = $_POST['note'];
$deadline = $_POST['deadline'];
$room = $_GET['room'];
date_default_timezone_set('Canada/Pacific');
$date = date('m/d/Y');
$action = $_GET['action'];
$assign = $_POST['assign'];
mysql_connect('localhost', 'root', '');
mysql_select_db('cic_rooms');

$queryRoom = mysql_query("SELECT * FROM room_list WHERE name = '$room' AND owner = '$owner' ");

if(mysql_num_rows($queryRoom) != 0){
	mysql_connect('localhost', 'root', '');
	mysql_select_db('cic_room_tools');

	if($action == 'insert'){
		$queryRoomTools = mysql_query("SELECT * FROM assignments WHERE room_name = '$room' AND owner = '$owner' ORDER BY id DESC LIMIT 1");
		$row = mysql_fetch_assoc($queryRoomTools);
		$assign_Num = $row['assign_number'];
		$assignNum = ($assign_Num + 1);
		mysql_query("INSERT INTO assignments (assign_number, room_name, note, date_posted, deadline, owner) VALUES('$assignNum', '$room', '$note', '$date', '$deadline', '$owner')") or die(mysql_error());
		mkdir("rooms/$room/assignments/assign_$assign");
	}elseif($action == 'update'){
		mysql_query("UPDATE assignments SET note = '$note', deadline = '$deadline' WHERE room_name = '$room' AND owner = $owner AND assign_number = '$assign'" ) or die(mysql_error());
	}

}






?>