<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

?>
<?php
if(isset($_POST['logout'])){
	session_destroy();
	header("Location: " . "../../.");
}

?>
<!DOCTYPE html>
<html>
  <head>
  	<link rel='stylesheet' href='../../css/home.css' />
  	<link rel='stylesheet' href='../../css/room.css' />
	<link rel="icon" type="image/png" href="../../assets/favicon.png" />
    <title><?php echo ucwords(basename(dirname(__FILE__))) . " Chat Room"; ?></title>

<style>
	textarea {
	    max-width: 300px; 
	    max-height: 100px;
	}

</style>
  </head>
<body>
 <div class="logo">
 <a href="../../home.php"><img src="../../assets/logocic.png" alt="Cic Logo" class="logocic"></a>
	 <form action="home.php" method="POST"  class="searchBar">
	 <input type="search" name="search" class="search">
	 <input type="hidden" value="hidden" name="hidden">
	 </form>

	 <form action="home.php" method="POST" class="logout">
	 <input type="submit" value="logout" name="logout">
	 </form>
	 
 </div>
<div class="main_content">
<section class="userNav">
<?php
	include("../../cic_db.php");
	$studentId = $_SESSION['cic_studentId'];
	$query = mysql_query("SELECT id, id_number, profession, firstname, lastname, profileurl FROM users WHERE id_number = '$studentId'") or die(mysql_error());
	$row = mysql_fetch_row($query);
	$id = $row['0'];
	$id_number = $row['1'];
	$profession = $row['2'];
	$firstname = $row['3'];
	$lastname = $row['4'];
	$profileurl = $row['5'];
	if(file_exists("../../users/$profileurl/avatar.jpg")) {
		echo "<img src='../../users/$profileurl/avatar.jpg' width='100px' height='100px' alt='User Avatar' />";
	}else{
		echo "<img src='../../assets/avatar.png' width='100px' height='100px' alt='User Avatar' />";
	
	}

?>


</div>

<div id="chat_container">

	<div id="senddiv">
		<span class="bubble"></span>
		<textarea id="message" rows="2" cols="30"></textarea>
		<input type="button" onClick="sendMessage()" value="Send">
	</div>

	<div id="scroll">

</div>
</div>
<script type="text/javascript" src="../../js/jquery1.10.min.js"></script>
<script type="text/javascript" src="js/showChat.js"></script>
<script type="text/javascript" src="js/postChat.js"></script>


</body>
</html>