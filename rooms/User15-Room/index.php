<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

?>
<?php
if(isset($_POST['logout'])){
	session_destroy();
	header("Location: " . "../../.");
}

?>
<!DOCTYPE html>
<html id='html'>
  <head>
  	<link rel='stylesheet' href='../../css/home.css' />
  	<link rel='stylesheet' href='../../css/room.css' />
	<link rel="icon" type="image/png" href="../../assets/favicon.png" />
    <title><?php echo ucwords(str_replace('-', ' ', ucwords(basename(dirname(__FILE__))))) . " Chat Room"; ?></title>

<style>
	textarea {
	    max-width: 300px; 
	    max-height: 100px;
	}
	#emoticonsHolder{
		width: 280px;
		height: 215px;
		border: solid 1px gray;
		position: absolute;
		margin-left: 265px;
		margin-top: 5px;
		z-index: 2;
	}
	.button:hover{
		cursor: pointer;
	}
	.dropButton{
		padding: 0 0 5px 5px;
	}
	.dropButton:hover{
		cursor: pointer;
		padding: 0 0 5px 5px;
	}
	#emoticonsHolder{
			position: absolute;
			width: 280px;
			height: 215px;
			padding: 0px;
			background: #FFFFFF;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
			border: #7F7F7F solid 2px;
			display: none;
	}

	#emoticonsHolder:after{
			content: '';
			position: absolute;
			border-style: solid;
			border-width: 0 9px 11px;
			border-color: #FFFFFF transparent;
			display: block;
			width: 0;
			z-index: 1;
			top: -11px;
			left: 7px;
	}

	#emoticonsHolder:before{
			content: '';
			position: absolute;
			border-style: solid;
			border-width: 0 10px 12px;
			border-color: #7F7F7F transparent;
			display: block;
			width: 0;
			z-index: 0;
			top: -14px;
			left: 6px;
	}
	.liRoomTitle{
		font-weight: bold;
		color: #800;
	}
	.liSubRoom{
		padding: 5px 0 0 5px;
		color: #fbac1b;
		cursor: pointer;
	}
	.container_box{
		background: rgb(54, 25, 25); /* Fall-back for browsers that don't
                                    support rgba */
	    background: rgba(54, 25, 25, .5);
	    position:fixed;
	    margin: auto;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 100%;
	    z-index: 99999;
	    display:none
	}
	.container_content{
		height: 80%;
		width: 60%;
		margin: 0 auto;
		margin-top: 6%;
		background-color: white;
	}
	#curricular{
		float: right;
		margin-top: 100px;
		margin-right: 60px;
		width: 20%;

	}
	#curricular section{
		color: #800;
		font-size: 14px;
		font-weight: bold;
		padding-left: 5px;
		background-color: #fbac1b;
		border-bottom: 1px solid #800;
	}
	#assignHolder{
		padding-left: 5px;
		height: 200px;
		overflow: auto;
		overflow-x: hidden;
	}
	.assignViewHolder{
		border: solid 1px;
		display: none;
		position: absolute;
		width: 290px;
		height: 350px;
		background-color: white;
		margin-left: -60px;
	}
	#quizHolder{
		padding-left: 5px;
		height: 200px;
		overflow: auto;
		overflow-x: hidden;
	}
</style>
  </head>
<body>
 <div class="logo">
 <a href="../../home.php"><img src="../../assets/logocic.png" alt="Cic Logo" class="logocic"></a>
	 <form action="home.php" method="POST"  class="searchBar">
	 <input type="search" id="search" class="closeDiv" placeholder='Search for Teachers, Students and Rooms...'>
	 <div id='searchResult'></div>
	 <input type="hidden" value="hidden" name="hidden">
	 </form>

	 <form action="home.php" method="POST" class="logout">
	 <input type="submit" value="logout" name="logout">
	 </form>
	 
 </div>
<div class="main_content">
<section class="userNav">
<?php
	include("../../cic_db.php");
	$studentId = $_SESSION['cic_studentId'];
	$roomName = basename(dirname(__FILE__));
	$query = mysql_query("SELECT id, id_number, profession, firstname, lastname, profileurl FROM users WHERE id_number = '$studentId'") or die(mysql_error());
	$row = mysql_fetch_row($query);
	$id = $row['0'];
	$id_number = $row['1'];
	$profession = $row['2'];
	$firstname = $row['3'];
	$lastname = $row['4'];
	$profileurl = $row['5'];
	if(file_exists("../../users/$profileurl/avatar.jpg")) {
		echo "<img src='../../users/$profileurl/avatar.jpg' width='100px' height='100px' alt='User Avatar' /><br />";
	}else{
		echo "<img src='../../assets/avatar.png' width='100px' height='100px' alt='User Avatar' /> <br />";
	
	}

?>
<?php 
if($profession == 'teacher'){
	echo "<ul><li class='liRoomTitle'>Room Tools<li>";
	echo "<li class='liSubRoom' onClick='showAssign()'>Create / Edit Assignments<li>";
	echo "<li class='liSubRoom' onclick='createQuiz()'>Create New Quiz<li>";
	echo "<li class='liSubRoom' onclick='viewRecord()'>View Records<li></ul>";
}
?>

</div>

<div id="chat_container">

	<div id="senddiv">
		<div class="buttonHolder">
		<textarea id="message" rows="2" cols="30"></textarea><img src="../../assets/emoticons/smile.png" class='dropButton' width='22' height="22" onclick="showEmoticons()" />
		<input type="button" onClick="sendMessage()" value="Send">
		<?php 
			date_default_timezone_set('Canada/Pacific');
			if($profession == 'student'){
				$attendance = 'attendance/attendance_'.date('m-d-Y').'.txt';
				if(!file_exists($attendance)){
					fopen($attendance, 'w');
					echo "<span class='attendanceBtn'><button>Time In</button><span>";
				}else{
					    if(strpos(file_get_contents("$attendance"),$studentId) !== false) {
     						echo "<span class='attendanceBtn'><button disabled>Present</button><span>";
   						}else{
   							echo "<span class='attendanceBtn'><button onclick='roomAttendance()'>Time In</button><span>";
   						}
				}
				
			} 

		?>
		</div>
		<div id='emoticonsHolder'>
			<img src="../../assets/emoticons/angry.gif" class='button' value='>.<' />
			<img src="../../assets/emoticons/balloon.gif" class='button' value=':balloon:' />
			<img src="../../assets/emoticons/beat.gif" class='button' value='<3' />
			<img src="../../assets/emoticons/bleeh.gif" class='button' value=':p' />
		    <img src="../../assets/emoticons/blush.gif" class='button' value='o"o' />
			<img src="../../assets/emoticons/broken.gif" class='button' value=':broken:' />
			<img src="../../assets/emoticons/bulb.gif" class='button' value=':bulb:' />
			<img src="../../assets/emoticons/bye.gif" class='button' value=':bye:' />
			<img src="../../assets/emoticons/cake.gif" class='button' value='-|]' />
			<img src="../../assets/emoticons/call.gif" class='button' value='[]' />
			<img src="../../assets/emoticons/cheer.gif" class='button' value='*"*' />
			<img src="../../assets/emoticons/clap.gif" class='button' value=':clap:' />
			<img src="../../assets/emoticons/coffee.gif" class='button' value='~o)' />
			<img src="../../assets/emoticons/conlaughf.gif" class='button' value='=p)' />
			<img src="../../assets/emoticons/cool.gif" class='button' value='B)' />
			<img src="../../assets/emoticons/cry.gif" class='button' value=":'(" />
			<img src="../../assets/emoticons/doubt.gif" class='button' value="l>" />
			<img src="../../assets/emoticons/drinks.gif" class='button' value=":drinks:" />
			<img src="../../assets/emoticons/drool.gif" class='button' value="**" />
			<img src="../../assets/emoticons/explode.gif" class='button' value=":boom:" />
			<img src="../../assets/emoticons/fight.gif" class='button' value=":fight:" />
			<img src="../../assets/emoticons/flyingkiss.gif" class='button' value=":*" />
			<img src="../../assets/emoticons/gift.gif" class='button' value=":gift:" />
			<img src="../../assets/emoticons/handshake.gif" class='button' value=":shake:" />
			<img src="../../assets/emoticons/hello.gif" class='button' value=":wave:" />
			<img src="../../assets/emoticons/knife.gif" class='button' value="|D" />
			<img src="../../assets/emoticons/lol.gif" class='button' value=":D" />
			<img src="../../assets/emoticons/moon.gif" class='button' value="*)" />
			<img src="../../assets/emoticons/no.gif" class='button' value=":nono:" />
			<img src="../../assets/emoticons/noidea.gif" class='button' value=":?" />
			<img src="../../assets/emoticons/note.gif" class='button' value="&db" />
			<img src="../../assets/emoticons/ok.gif" class='button' value=":k" />
			<img src="../../assets/emoticons/peace.gif" class='button' value=":y" />
			<img src="../../assets/emoticons/please.gif" class='button' value=":{" />
			<img src="../../assets/emoticons/poop.gif" class='button' value="~@" />
			<img src="../../assets/emoticons/rain.gif" class='button' value=":rain:" />
			<img src="../../assets/emoticons/rainbow.gif" class='button' value="((8" />
			<img src="../../assets/emoticons/sad.gif" class='button' value=":(" />
			<img src="../../assets/emoticons/scared.gif" class='button' value=":0" />
			<img src="../../assets/emoticons/shock.gif" class='button' value=":shock:" />
			<img src="../../assets/emoticons/snail.gif" class='button' value=":snail:" />
			<img src="../../assets/emoticons/snob.gif" class='button' value=":snob:" />
			<img src="../../assets/emoticons/sob.gif" class='button' value="TT" />
			<img src="../../assets/emoticons/star.gif" class='button' value=":star:" />
			<img src="../../assets/emoticons/sun.gif" class='button' value=":sun:" />
			<img src="../../assets/emoticons/wine.gif" class='button' value=":wine:" />
			<img src="../../assets/emoticons/wink.gif" class='button' value=";)" />
		</div>
	</div>

	<div id="scroll">

	</div>

	<div class='container_box'>
		<div class='container_content'>

		</div>
	</div>

</div>

<div id='curricular'>
	<div class='assignments'>
	<div class='assignViewHolder'>

	</div>
	<section>Assignments:</section>
		<div id='assignHolder'></div>
	
	</div>

	<div class='quiz'>
	<section>Quizes:</section>
		<div id='quizHolder'></div>
	</div>
</div>


<script type="text/javascript" src="../../js/jquery1.10.min.js"></script>
<script type="text/javascript" src="../../js/search2.js"></script>
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="js/showChat.js"></script>
<script type="text/javascript" src="js/postChat.js"></script>
<script type="text/javascript" src="jquery.Upload.js"></script>
<script type="text/javascript">



$(function () {
    $('.profile_button').click(function () {
        $('.form_box').toggle();
    })
})


function showEmoticons(){

	   $('#emoticonsHolder').fadeToggle();
}
$(document).ready(function(){

	$('.button').click(function(){
		var emoticons = $(this).attr('value');
		var message = $('#message').val();
		$("#message").focus();
		$('#message').val(message + ' ' + emoticons + ' ');
		showEmoticons();

	});
});

var roomName = "<?php echo $roomName ?>";

function showAssign(){

	var path = '../../roomTools.php?create=assign&roomName='+roomName;
	$('.container_content').html('');
	$('.container_box').toggle();
	$('.container_content').load(path);

}
function createQuiz(){
	$('.container_content').html('');
	var quizPath = '../../roomTools.php?create=quiz&roomName='+roomName;
	$('.container_box').toggle();
	$('.container_content').load(quizPath);

}
function viewRecord(){
	$('.container_content').html('');
	var path = '../../roomTools.php?create=records&roomName='+roomName;
	$('.container_box').toggle();
	$('.container_content').load(path);
}

var viewAssign = "../../roomActivities.php?action=viewAssign&room="+roomName;
	$('#assignHolder').load(viewAssign);

function viewAssignment(viewAssign_id){
	var viewAssignId = viewAssign_id;
	var assignArr = viewAssignId.split(":");
	var uploadAssign = "upload.php?action=uploadAssign&room="+roomName+"&assignNum="+assignArr[1];
	$('.assignViewHolder').toggle();
	$('.assignViewHolder').load(uploadAssign);

}

function closeUpload(){
		$('.assignViewHolder').toggle();
}
function roomAttendance(){

	var room = roomName;
	$.post('../../roomAttendance.php', {room:room} );
	$('.attendanceBtn').html('<button disabled>Present</button>');
}
</script>

</body>
</html>