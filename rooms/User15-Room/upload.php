<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}




$room = $_GET['room'];
$assignNum  = $_GET['assignNum'];
$action = $_GET['action'];
mysql_connect('localhost', 'root', '');
mysql_select_db('cic_room_tools');

$query = mysql_query("SELECT * FROM assignments WHERE room_name = '$room' AND assign_number = '$assignNum'");
$row = mysql_fetch_assoc($query);
$note = $row['note'];
date_default_timezone_set('Canada/Pacific');
$dateNow = date('m/d/Y');
$deadline =$row['deadline'];

echo "<div style='background-color:#fbac1b;color:#800;border-bottom:#800;font-weight:bold;height:28px;display:block;'>Submit Assignment #: $assignNum <button style='float:right;' onclick='closeUpload()'>Close</button></div>";
echo "<div style='border-bottom:solid 1px gray; height:140px;overflow:auto;overflow-x:hidden;padding:5px 5px 0 5px;'>".$note."</div>";

if($dateNow <= $deadline){
	echo "<div style='text-align: center;'><br />Supported formats: .doc, .docx, .txt, .zip, .rar, .xls";
	echo "<br />All files will be renamed to yourname_id_number_assign_number<br /><br />";
  echo "<div>";
	echo "<form id='assigForm' method='post' enctype='multipart/form-data' action='../../uploadFile.php?assignNum=$assignNum&room=$room'>";
	echo "Upload your Assignment <input type='file' name='assignFile' id='assignFile' />";
  echo "</form>";
  echo "<div id='preview'>";
  echo "</div>";
  echo "</div>";

            
            
            
            
}else{
echo "<div style='text-align:center;margin-top:50px;'>";
	echo "<span style='font-style:italic;color:red;'>Submission for this Assignment already Expired</span>";
echo "</div>";
}






?>

<script>
 $(document).ready(function() { 
    
            $('#assignFile').live('change', function(){ 
          $("#preview").html('');
          $("#preview").html('<img src="../../assets/ajax-loader.gif" alt="Uploading...."/>');
        $("#assigForm").ajaxForm({
            target: '#preview'
        }).submit();
    
      });
        }); 

</script>