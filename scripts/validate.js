function isValidEmail(str) {
return (str.indexOf(".") > 2) && (str.indexOf("@") > 0);
}


function CheckData()
{

if (document.getElementById('error').style.display == "block")
document.getElementById('error').style.display = "none";


with(document.registration)
{

name.value = trim(name.value);
phone.value = trim(phone.value);
course.value = trim(course.value);
email.value = trim(email.value);
profession.value = trim(profession.value);
if(name.value == "")
{


document.getElementById('error').style.display = "block";
document.getElementById('errorMessage').innerText = "Please Enter Your First Name";

name.focus();
return false;
}



if(phone.value == "")
{


document.getElementById('error').style.display = "block";
document.getElementById('errorMessage').innerText = "Please Enter Your Phone no.";


phone.focus();
return false;
}



if(course.value == "")
{


document.getElementById('error').style.display = "block";
document.getElementById('errorMessage').innerText = "Please Enter Your Course.";


course.focus();
return false;
}




if(email.value == "")
{


document.getElementById('error').style.display = "block";
document.getElementById('errorMessage').innerText = "Please a Valid Email.";


email.focus();
return false;
}
if(profession.value == "")
{


document.getElementById('error').style.display = "block";
document.getElementById('errorMessage').innerText = "Im a: Select an Option.";


profession.focus();
return false;
}
}
return true;
}


function trim(sValue)
{
	if (sValue == null)
		return null;

	for (var i = 0; sValue.charAt(i) == " "; i ++);
	
	sValue = sValue.substring(i,sValue.length);

	if (sValue == null)
		return null;

	for(var i = (sValue.length-1); sValue.charAt(i) == " "; i --);
	
	return sValue.substring(0, (i + 1));
}
