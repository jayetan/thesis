-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2014 at 07:43 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cic_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` int(11) NOT NULL,
  `profession` varchar(50) NOT NULL,
  `course` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `profileurl` varchar(100) NOT NULL,
  `about` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `rank` varchar(50) NOT NULL,
  `activated` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_number`, `profession`, `course`, `firstname`, `lastname`, `gender`, `email`, `password`, `profileurl`, `about`, `status`, `rank`, `activated`) VALUES
(1, 1, 'admin', 'BSIT', 'CIC', 'COLLEGE', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'c4ca4238a0b923820dcc509a6f75849b', '', 'idle', 'admin', 'yes'),
(3, 2, 'student', '', 'qwerty', 'qwerty', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'c81e728d9d4c2f636f067f89cc14862c', '', '0', '', ''),
(4, 3, 'student', '', 'qwerty', 'qwerty', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '', '0', '', ''),
(5, 4, 'teacher', '', 'user4', 'user4', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'a87ff679a2f3e71d9181a67b7542122c', '', '0', '', ''),
(6, 5, 'teacher', '', 'qwerty', 'qwerty', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'e4da3b7fbbce2345d7772b0674a318d5', '', '0', '', ''),
(7, 6, 'student', '', 'jei', 'tan', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '1679091c5a880faf6fb5e6087eb1b2dc', '', '0', '', ''),
(8, 7, 'student', '', 'lei', 'tan', 'female', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '8f14e45fceea167a5a36dedd4bea2543', '', '0', '', ''),
(9, 9, 'student', 'bsit', 'name9', 'surname9', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '45c48cce2e2d7fbdea1afc51c7c6ad26', '', '0', '', ''),
(10, 10, 'teacher', 'bsit', 'user10', 'last10', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'd3d9446802a44259755d38e6d163e820', '', '0', '', ''),
(11, 11, 'teacher', 'bsit', 'user11', 'last11', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '6512bd43d9caa6e02c990b0a82652dca', '', '0', '', ''),
(12, 12, 'teacher', 'bsit', 'jat', 'tan', 'male', 'jayetan12@ymail.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'c20ad4d76fe97759aa27a0c99bff6710', '', 'idle', '', 'yes'),
(13, 13, 'teacher', 'bsit', 'user13', 'user13', 'male', 'qwerty@qwert.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'c51ce410c124a10e0db5e4b97fc2af39', '', 'offline', '', 'yes'),
(14, 14, 'student', 'BSBA', 'userfemale14', 'lastname14', 'female', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'aab3238922bcc25a6f606eb525ffdc56', '', 'offline', '', 'yes'),
(17, 15, 'teacher', 'bsit', 'user15', 'last15', 'male', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '9bf31c7ff062936a96d3c8bd1f8f2ff3', '', 'online', '', 'yes'),
(18, 17, 'student', 'bsit', 'charo', 'charo', 'female', 'qwerty@qwerty.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '70efdf2ec9b086079795c442636b55fb', '', 'offline', '', 'yes'),
(19, 18, 'student', 'BSHRM', 'User18', 'Last18', 'female', 'user@user.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '6f4922f45568161a8cdf4ad2299f6d23', '', 'online', '', 'yes'),
(20, 11420004, 'student', 'BSIT', 'jaye alejandro', 'tan', 'male', 'jayetan12@ymail.com', '3ea096af315b5fc2ab22f7eacf17c21112322dfb', '3083fca339825b7f7ff798acab4b2e60', '', 'online', '', 'yes'),
(21, 10410101, 'student', 'BSIT', 'Jvianney', 'Delos Reyes', 'male', 'jvianneydelosreyes19@yahoo.com', 'fe703d258c7ef5f50b71e06565a65aa07194907f', '5eaa4d45ff1d3301c35121081c928022', '', '', '', 'no'),
(22, 26, 'student', 'BSIT', 'user26', 'last26', 'male', 'jayetan12@ymail.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '4e732ced3463d06de0ca9a15b6153677', '', 'offline', '', 'yes'),
(23, 25, 'teacher', 'BSIT', 'alvin', 'pogi', 'male', 'sample@sample.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '8e296a067a37563370ded05f5a3bf3ec', '', 'online', '', 'yes'),
(24, 27, 'teacher', 'BSIT', 'user27', 'last27', 'male', 'test@test.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', '02e74f10e0327ad868d138f2b4fdd6f0', '', 'offline', '', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wall`
--

CREATE TABLE IF NOT EXISTS `wall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` int(11) NOT NULL,
  `post` text NOT NULL,
  `time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `wall`
--

INSERT INTO `wall` (`id`, `id_number`, `post`, `time`) VALUES
(1, 11, 'testing', '0'),
(2, 11, 'testing', 'Nov-12-13 4:40:17'),
(3, 11, 'testing', 'Nov:12:13:4:41:03'),
(4, 11, 'testing', 'Nov:12:13:4:42:06'),
(5, 11, 'testing', 'Nov:12:13:4:42:30'),
(6, 11, 'fdsfdss fdsfsdfsd sfsdfdfs sfsdpfslkmds  fsd;f sldkfmsdf s sfsd;mslkmsdlkmsdk f sfksflksmfskmfskf flmfsdlkmfsdlkfmsdlk slkfmdslfmdslkmfds\r\n', 'Nov:12:13:5:07:18'),
(8, 13, 'Hi there!', 'Dec:06:13:5:14:29'),
(9, 13, 'Sample post..', 'Dec:06:13:6:32:46'),
(10, 13, 'test post', 'Dec:06:13:6:46:17'),
(11, 12, 'test post', 'Dec:22:13:4:37:33'),
(12, 12, '26+2', 'Dec:25:13:6:33:59'),
(13, 12, 'testing', 'Dec:25:13:7:44:08'),
(14, 12, '5465464', 'Dec:25:13:12:33:58'),
(15, 12, 'testing', 'Dec:25:13:12:35:19'),
(16, 11, 'Merry Christmas!', 'Dec:25:13:12:40:10'),
(17, 13, 'testing', 'Dec-25-13 15:34:02'),
(18, 12, 'post test', 'Dec:27:13:16:35:07'),
(19, 12, 'testing (date and time)', 'Dec-31-2013 5:57:15'),
(20, 13, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'Jan-02-2014 9:50:32'),
(21, 17, 'rsgfxgfx', 'Jan-17-2014 6:19:56'),
(22, 14, 'fsfsdfsd', 'Jan-17-2014 6:21:12'),
(23, 14, '"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."', 'Jan-31-2014 3:43:38'),
(24, 14, 'testing update w/ color', 'Feb-06-2014 6:27:27'),
(25, 12, 'test update', 'Feb-06-2014 6:28:48'),
(27, 1, 'testing update', 'Feb-07-2014 5:56:54'),
(28, 27, 'test', 'Feb-20-2014 1:52:04');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
