-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2014 at 07:44 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cic_privatemessages`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `message_id` varchar(20) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `date_posted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`message_id`, `sender`, `receiver`, `owner`, `count`, `date_posted`) VALUES
('17:12', 17, 12, 17, 0, 1392150517),
('17:12', 12, 17, 12, 3, 1392150468),
('14:12', 14, 12, 14, 0, 1392123655),
('14:12', 12, 14, 12, 3, 1392150301),
('26:12', 26, 12, 26, 1, 1392926055),
('26:12', 12, 26, 12, 0, 1392926092),
('25:12', 25, 12, 25, 0, 1392888852),
('25:12', 12, 25, 12, 1, 1392888852);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` varchar(20) NOT NULL,
  `sender` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message_id`, `sender`, `message`, `date`) VALUES
(1, '12:14', 12, 'testing messages..', '2014-02-10 05:27:18'),
(2, '17:12', 17, 'testing messages', '2014-02-11 19:35:54'),
(3, '17:12', 17, 'testing', '2014-02-11 20:23:19'),
(4, '14:12', 14, 'sdfsdfsda', '2014-02-11 20:25:02'),
(5, '17:12', 17, 'test msg', '2014-02-11 20:27:48'),
(6, '17:12', 17, 'test again', '2014-02-11 20:28:37'),
(7, '14:12', 14, 'testing order', '2014-02-12 00:58:34'),
(8, '14:12', 14, 'vjhvj', '2014-02-12 01:00:55'),
(9, '26:12', 26, 'hi!', '2014-02-20 19:54:15'),
(10, '26:12', 12, 'hello!', '2014-02-20 19:54:52'),
(11, '25:12', 25, 'hi', '2014-02-20 21:34:12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
