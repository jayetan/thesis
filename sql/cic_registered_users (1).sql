-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2014 at 07:48 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cic_registered_users`
--

-- --------------------------------------------------------

--
-- Table structure for table `1`
--

CREATE TABLE IF NOT EXISTS `1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `1`
--

INSERT INTO `1` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 1, 'yes', NULL),
(2, 12, 'yes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `11`
--

CREATE TABLE IF NOT EXISTS `11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `11`
--

INSERT INTO `11` (`id`, `friends`, `status`) VALUES
(1, 11, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `12`
--

CREATE TABLE IF NOT EXISTS `12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `12`
--

INSERT INTO `12` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 12, 'yes', 0),
(20, 14, 'yes', 12),
(24, 17, 'yes', 12),
(25, 1, 'yes', 1),
(26, 15, 'pending', 15),
(27, 18, 'yes', 18),
(28, 26, 'yes', 12),
(29, 25, 'yes', 12);

-- --------------------------------------------------------

--
-- Table structure for table `13`
--

CREATE TABLE IF NOT EXISTS `13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `13`
--

INSERT INTO `13` (`id`, `friends`, `status`) VALUES
(1, 13, 'yes'),
(3, 14, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `14`
--

CREATE TABLE IF NOT EXISTS `14` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(50) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `14`
--

INSERT INTO `14` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 14, 'yes', 0),
(24, 12, 'yes', 12),
(25, 0, 'pending', 14);

-- --------------------------------------------------------

--
-- Table structure for table `15`
--

CREATE TABLE IF NOT EXISTS `15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `15`
--

INSERT INTO `15` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 15, 'yes', NULL),
(2, 12, 'pending', 15);

-- --------------------------------------------------------

--
-- Table structure for table `17`
--

CREATE TABLE IF NOT EXISTS `17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `17`
--

INSERT INTO `17` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 17, 'yes', NULL),
(3, 12, 'yes', 12);

-- --------------------------------------------------------

--
-- Table structure for table `18`
--

CREATE TABLE IF NOT EXISTS `18` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `18`
--

INSERT INTO `18` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 18, 'yes', NULL),
(2, 12, 'yes', 18);

-- --------------------------------------------------------

--
-- Table structure for table `25`
--

CREATE TABLE IF NOT EXISTS `25` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `25`
--

INSERT INTO `25` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 25, 'yes', NULL),
(2, 12, 'yes', 12);

-- --------------------------------------------------------

--
-- Table structure for table `26`
--

CREATE TABLE IF NOT EXISTS `26` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `26`
--

INSERT INTO `26` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 26, 'yes', NULL),
(2, 12, 'yes', 12);

-- --------------------------------------------------------

--
-- Table structure for table `27`
--

CREATE TABLE IF NOT EXISTS `27` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `27`
--

INSERT INTO `27` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 27, 'yes', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `10410101`
--

CREATE TABLE IF NOT EXISTS `10410101` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `10410101`
--

INSERT INTO `10410101` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 10410101, 'yes', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `11420004`
--

CREATE TABLE IF NOT EXISTS `11420004` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friends` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `requestby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `11420004`
--

INSERT INTO `11420004` (`id`, `friends`, `status`, `requestby`) VALUES
(1, 11420004, 'yes', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
