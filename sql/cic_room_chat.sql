-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2014 at 07:48 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cic_room_chat`
--

-- --------------------------------------------------------

--
-- Table structure for table `cic-lounge`
--

CREATE TABLE IF NOT EXISTS `cic-lounge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `post_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `cic-lounge`
--

INSERT INTO `cic-lounge` (`id`, `user`, `post`, `post_date`) VALUES
(1, '12', 'hello', 'Jan-03-2014 8:38:31'),
(2, '12', 'testing', 'Jan-03-2014 8:38:54'),
(3, '13', 'Hi There!', 'Jan-03-2014 9:09:05'),
(4, '12', 'testing :wave:', 'Jan-06-2014 10:10:37'),
(5, '12', ':D', 'Jan-07-2014 2:07:36'),
(6, '12', 'elow po', 'Jan-07-2014 2:22:06'),
(7, '12', '', 'Jan-07-2014 2:22:07'),
(8, '12', '(y)\n', 'Jan-07-2014 2:22:18'),
(9, '12', ':p', 'Jan-07-2014 2:22:34'),
(10, '12', 'test', 'Jan-08-2014 1:11:20'),
(11, '12', 'ssfsdfsd', 'Jan-08-2014 1:45:00'),
(12, '12', 'test', 'Jan-08-2014 11:02:29'),
(13, '12', '61561', 'Jan-09-2014 12:03:37'),
(14, '12', 'kjn', 'Jan-09-2014 12:06:48'),
(15, '12', 'jh ', 'Jan-09-2014 12:09:00'),
(16, '12', 'fghgfhg', 'Jan-09-2014 12:24:59'),
(17, '12', 'blah!', 'Jan-09-2014 12:30:48'),
(18, '12', 'Testing Again!', 'Jan-09-2014 2:04:19'),
(19, '12', 'fsdfsdfsd', 'Jan-09-2014 2:05:19'),
(20, '12', 'sdsadsa', 'Jan-09-2014 2:09:28'),
(21, '12', 'dsadsad', 'Jan-09-2014 2:16:12'),
(22, '12', 'csdfsd', 'Jan-09-2014 2:20:09'),
(23, '12', 'sadsad', 'Jan-09-2014 2:27:59'),
(24, '12', 'new post', 'Jan-09-2014 2:39:21'),
(25, '12', 'test', 'Jan-09-2014 2:45:20'),
(26, '12', 'ajaxified!', 'Jan-09-2014 2:53:18'),
(27, '12', 'firefox test', 'Jan-09-2014 3:20:25'),
(28, '12', 'chrome test', 'Jan-09-2014 3:21:16'),
(29, '12', 'dsfsdfsd', 'Jan-09-2014 3:23:11'),
(30, '12', 'sdasda', 'Jan-09-2014 3:24:25'),
(31, '12', 'dsadas', 'Jan-09-2014 3:26:47'),
(32, '13', 'dsadsa', 'Jan-09-2014 3:36:03'),
(33, '13', 'dsadsada', 'Jan-09-2014 3:36:14'),
(34, '13', 'sadsadas', 'Jan-09-2014 7:46:08'),
(35, '13', '23112321', 'Jan-09-2014 3:02:01'),
(36, '13', 'dgfdgd', 'Jan-09-2014 3:12:37'),
(37, '13', 'wqewqewq', 'Jan-10-2014 10:53:52'),
(38, '13', ':*', 'Jan-10-2014 10:54:33'),
(39, '12', ':wave:', 'Jan-10-2014 8:49:36'),
(40, '17', 'Testing', 'Jan-19-2014 9:07:46'),
(41, '1', 'testing icon :bye: ', 'Jan-27-2014 9:53:31'),
(42, '12', '', 'Feb-06-2014 10:30:57'),
(43, '12', ' :D ', 'Feb-13-2014 10:49:56'),
(44, '12', 'hi  :bye: ', 'Feb-20-2014 11:41:17'),
(45, '26', 'hello! :D ', 'Feb-20-2014 11:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `hhr`
--

CREATE TABLE IF NOT EXISTS `hhr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `post_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lounge`
--

CREATE TABLE IF NOT EXISTS `lounge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `post_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `lounge`
--

INSERT INTO `lounge` (`id`, `user`, `post`, `post_date`) VALUES
(1, '12', 'testing', 'Jan-03-2014 8:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `room-320`
--

CREATE TABLE IF NOT EXISTS `room-320` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `post_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `room-320`
--

INSERT INTO `room-320` (`id`, `user`, `post`, `post_date`) VALUES
(1, '25', 'hi :wave: ', 'Feb-20-2014 1:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `test-new-room`
--

CREATE TABLE IF NOT EXISTS `test-new-room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `post_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test-room`
--

CREATE TABLE IF NOT EXISTS `test-room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `post_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `testroom`
--

CREATE TABLE IF NOT EXISTS `testroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `post_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user15-room`
--

CREATE TABLE IF NOT EXISTS `user15-room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `post_date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
