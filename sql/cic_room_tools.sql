-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2014 at 07:49 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cic_room_tools`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assign_number` int(11) NOT NULL,
  `room_name` varchar(20) NOT NULL,
  `note` text NOT NULL,
  `date_posted` varchar(20) NOT NULL,
  `deadline` varchar(20) NOT NULL,
  `owner` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `assign_number`, `room_name`, `note`, `date_posted`, `deadline`, `owner`) VALUES
(1, 1, 'cic-lounge', '"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."', '02/10/2014', '02/15/2014', 12),
(2, 2, 'cic-lounge', 'testing', '02/12/2014', '02/15/2014', 12),
(3, 3, 'cic-lounge', 'asssign 3', '02/12/2014', '03/16/2014', 12),
(4, 4, 'cic-lounge', 'assign. 4', '02/20/2014', '12/12/2014', 12),
(5, 5, 'cic-lounge', 'please answer pages 15-25', '02/20/2014', '02/21/2014', 12),
(6, 1, 'testroom', 'assign 1', '02/20/2014', '12/12/2014', 27),
(7, 1, 'test-new-room', 'assign test', '02/20/2014', '12/12/2014', 12);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_db`
--

CREATE TABLE IF NOT EXISTS `quiz_db` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `room_name` varchar(50) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `first_choice` text NOT NULL,
  `second_choice` text NOT NULL,
  `third_choice` text NOT NULL,
  `deadline` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `quiz_db`
--

INSERT INTO `quiz_db` (`id`, `quiz_id`, `owner`, `room_name`, `question`, `answer`, `first_choice`, `second_choice`, `third_choice`, `deadline`) VALUES
(1, 1, 12, 'cic-lounge', 'a:5:{i:0;s:2:"q1";i:1;s:2:"q2";i:2;s:2:"q3";i:3;s:2:"q4";i:4;s:2:"q5";}', 'a:5:{i:0;s:1:"a";i:1;s:1:"b";i:2;s:1:"c";i:3;s:1:"b";i:4;s:1:"a";}', 'a:5:{i:0;s:1:"1";i:1;s:1:"1";i:2;s:1:"8";i:3;s:1:"9";i:4;s:1:"5";}', 'a:5:{i:0;s:1:"2";i:1;s:1:"2";i:2;s:1:"4";i:3;s:1:"4";i:4;s:1:"5";}', 'a:5:{i:0;s:1:"3";i:1;s:1:"5";i:2;s:1:"3";i:3;s:1:"7";i:4;s:1:"7";}', '03_12_2014'),
(2, 2, 12, 'cic-lounge', 'a:10:{i:0;s:6:"rwerwe";i:1;s:5:"sdfsd";i:2;s:6:"dsfsdf";i:3;s:6:"fsdfsd";i:4;s:8:"sdfsdfsd";i:5;s:6:"fsdfsd";i:6;s:6:"dfsdfs";i:7;s:6:"sdfsdf";i:8;s:6:"sdfsdf";i:9;s:9:"fsfsdfsdf";}', 'a:10:{i:0;s:1:"a";i:1;s:1:"b";i:2;s:1:"a";i:3;s:1:"c";i:4;s:1:"a";i:5;s:1:"a";i:6;s:1:"c";i:7;s:1:"c";i:8;s:1:"b";i:9;s:1:"a";}', 'a:10:{i:0;s:5:"dsfsd";i:1;s:4:"fsfs";i:2;s:4:"fsdf";i:3;s:4:"sfsd";i:4;s:5:"sdfsd";i:5;s:6:"sdfsdf";i:6;s:5:"fssdf";i:7;s:6:"sdfsdf";i:8;s:6:"sdfsdf";i:9;s:6:"fsdfsd";}', 'a:10:{i:0;s:5:"sdfsd";i:1;s:6:"sdfsdf";i:2;s:6:"sdfsdf";i:3;s:6:"fsdfsd";i:4;s:5:"sdfsd";i:5;s:8:"sdsdfsdf";i:6;s:5:"fsdfs";i:7;s:3:"sdf";i:8;s:5:"sdfsd";i:9;s:5:"sdfsd";}', 'a:10:{i:0;s:5:"fsdfs";i:1;s:6:"sdfsdf";i:2;s:3:"sdf";i:3;s:5:"fsdfs";i:4;s:6:"fsdfsd";i:5;s:4:"sdfs";i:6;s:5:"sdfsd";i:7;s:4:"sfsd";i:8;s:6:"fsdfsd";i:9;s:5:"sdfsd";}', '12_12_2014'),
(3, 1, 12, 'test-new-room', 'a:5:{i:0;s:5:"fdsaf";i:1;s:8:"asdfasdf";i:2;s:8:"sadfsdaf";i:3;s:8:"sadfsdaf";i:4;s:5:"fsdaf";}', 'a:5:{i:0;s:1:"a";i:1;s:1:"c";i:2;s:1:"b";i:3;s:1:"c";i:4;s:1:"b";}', 'a:5:{i:0;s:10:"sdfasdfsdf";i:1;s:7:"sadfsda";i:2;s:4:"sdaf";i:3;s:6:"sdafsd";i:4;s:4:"sdaf";}', 'a:5:{i:0;s:6:"sdfasd";i:1;s:5:"fasdf";i:2;s:7:"sadfsad";i:3;s:5:"sdafs";i:4;s:7:"sdafasd";}', 'a:5:{i:0;s:5:"fsadf";i:1;s:7:"sadfsda";i:2;s:5:"fsadf";i:3;s:9:"dafsadfsd";i:4;s:9:"fasdfasdf";}', '12_12_2014');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_scores`
--

CREATE TABLE IF NOT EXISTS `quiz_scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `room` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `quiz_scores`
--

INSERT INTO `quiz_scores` (`id`, `id_number`, `quiz_id`, `score`, `room`) VALUES
(4, 12, 1, 4, 'cic-lounge'),
(6, 18, 1, 5, 'cic-lounge');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
