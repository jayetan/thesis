-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2014 at 07:50 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cic_rooms`
--

-- --------------------------------------------------------

--
-- Table structure for table `room_list`
--

CREATE TABLE IF NOT EXISTS `room_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `room_list`
--

INSERT INTO `room_list` (`id`, `owner`, `name`, `date`) VALUES
(2, '12', 'cic-lounge', 'Jan-03-2014 7:56:48'),
(3, '12', 'Lounge', 'Jan-03-2014 8:34:54'),
(4, '12', 'hhr', 'Jan-07-2014 2:24:03'),
(5, '15', 'User15-Room', 'Feb-08-2014 8:35:26'),
(6, '12', 'test-room', 'Feb-18-2014 9:53:10'),
(7, '25', 'room-320', 'Feb-20-2014 1:29:19'),
(8, '27', 'testroom', 'Feb-20-2014 1:52:24'),
(9, '12', 'test-new-room', 'Feb-20-2014 10:32:13');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
