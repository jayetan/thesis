<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = ".";
if (!((isset($_SESSION['cic_studentId'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['cic_studentId'], $_SESSION['log_studentId'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
ini_set('max_execution_time', 0);
$studentId = $_SESSION['cic_studentId'];
$userFolder = md5($studentId);
$path = "users/$userFolder";


  $valid_formats = array("jpg", "png", "gif", "bmp");
  if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
    {
      $name = $_FILES['photoimg']['name'];
      $size = $_FILES['photoimg']['size'];
      
      if(strlen($name))
        {
          list($txt, $ext) = explode(".", $name);
          if(in_array($ext,$valid_formats))
          {
          if($size<(1024*1024))
            {
              $actual_image_name = "avatar.".$ext;
              $tmp = $_FILES['photoimg']['tmp_name'];
              if(move_uploaded_file($tmp, $path.$actual_image_name))
                {
                  
                  echo "<img src='uploads/avatar.jpg'  class='preview'>";
                }
              else
                echo "failed";
            }
            else
            echo "Image file size max 1 MB";          
            }
            else
            echo "Invalid file format.."; 
        }
        
      else
        echo "Please select image..!";
        
      exit;
    }

?>