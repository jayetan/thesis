function CheckData()
{



if (document.getElementById('error').style.display == "block")
document.getElementById('error').style.display = "none";


with(document.registrationForm)
{

studentId.value = trim(studentId.value);
profession.value = trim(profession.value);
course.value = trim(course.value);
firstName.value = trim(firstName.value);
lastName.value = trim(lastName.value);
gender.value = trim(gender.value);
email.value = trim(email.value);
newPassword.value = trim(newPassword.value);
repeatPassword.value = trim(repeatPassword.value);

if(studentId.value == "")
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Please Enter Your ID Number").fadeIn('slow');

studentId.focus();
return false;
}



if(profession.value == "")
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Im a: Select an Option.").fadeIn('slow');


profession.focus();
return false;
}



if(course.value == "")
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Please Provide Your Course.").fadeIn('slow');


course.focus();
return false;
}



if(firstName.value == "")
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Please Provide Your Name.").fadeIn('slow');

firstName.focus();
return false;
}


if(lastName.value == "")
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Please Provide Your Last Name.").fadeIn('slow');


lastName.focus();
return false;
}


if(gender.value == "")
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Please Provide Your Gender.").fadeIn('slow');

gender.focus();
return false;
}


if(email.value == "")
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Please Provide Your Email.").fadeIn('slow');


email.focus();
return false;
}
var email = $('#email').val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(email.value) == false) 
        {
			document.getElementById('error').style.display = "block";
			//document.getElementById('errorMessage').innerHtml = "Invalid Email Address";
			$('.errorMessage').html("Invalid Email Address").fadeIn('slow');
			email.focus();
            return false;
        }



if(newPassword.value == ""  && repeatPassword.value =="")
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Please Provide Password").fadeIn('slow');


newPassword.focus();
return false;
}




if(newPassword.value != repeatPassword.value)
{


document.getElementById('error').style.display = "block";
$('.errorMessage').html("Password Did not Match!.").fadeIn('slow');


repeatPassword.focus();
return false;
}


}
return true;
}


function trim(sValue)
{
	if (sValue == null)
		return null;

	for (var i = 0; sValue.charAt(i) == " "; i ++);
	
	sValue = sValue.substring(i,sValue.length);

	if (sValue == null)
		return null;

	for(var i = (sValue.length-1); sValue.charAt(i) == " "; i --);
	
	return sValue.substring(0, (i + 1));
}
