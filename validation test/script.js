
// handle the form submit event
function prepareEventHandlers() {
	document.getElementById("formSubmit").onsubmit = function() {
		// prevent a form from submitting if no email.
		if (document.getElementById("nameLog").value == "") {
			document.getElementById("errorMessage").innerHTML = "Please provide a Name!";
			// to STOP the form from submitting
			return false;
		} else {
			// reset and allow the form to submit
			document.getElementById("errorMessage").innerHTML = "";
			return true;
		}
	};
}

// when the document loads
window.onload =  function() {
	prepareEventHandlers();
};

